//récupération des données de l'éllipsoid envoyé par djnago
const ellips_data = JSON.parse(document.getElementById('geoid_ellips_data').textContent);
//récuprération du div de l'éllipsoid
let geoid_etopo = document.getElementById('geoid_ellips');

// donnée brute et graphique de l'geoid_etopo
let data = [{
  x: ellips_data[0], y: ellips_data[1], z: ellips_data[2],
  surfacecolor  : ellips_data[3],
  type: 'surface',
  showscale: true,
  colorscale : 'Red',
  lighting: {ambient: 0.8}
  },
];

let layout = {
  // title: 'geoid_etopo ',
  title:{text:"geoid_etopo", font:{family:"Arial Black", color:"blue", size:16}, xanchor:"right"},
  autosize: false,
   width: 600,
   height: 550,
   margin: {
    l: 5,
    r: 5,
    b: 15,
    t: 25,
  },
  pad: {
    l: 5,
    r: 5,
    b: 15,
    t: 0,
  }
  // scene:{
  // aspectmode:'cube',
  //  domain:{row:0, column:0}
  // }
};

Plotly.react(geoid_etopo, data, layout);



