var mapListePoint = L.map('mapListePoint').setView([51.505, -0.09], 5);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox/satellite-streets-v9',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: 'pk.eyJ1IjoiYXhlbGxzYyIsImEiOiJja3V5MHJsOHMwemhsMnByZnJraHo3Y3NxIn0.-RSeYnibv28gdgJtmyLbMw'
      }).addTo(mapListePoint);
var myLayerGroupForListPoint=L.featureGroup().addTo(mapListePoint);

var mutliPointInput = document.getElementById("mutliPointInput");
var mouseCoord2 = document.getElementById("mouseCoord2");

var latPlotly  = [];
var lngPlotly  = [];
var obsPlotly = [];
var numListePoint;
var currentListePoint;

mapListePoint.on('mousemove', (e) => {
      // console.log(e.latlng.lat);
      mouseCoord2.innerText = "Longitude:" + e.latlng.lng.toFixed(6) + ", Latitude:" + e.latlng.lat.toFixed(6);
      });

      mapListePoint.on('click', (e)=>{
      var greenIcon = new L.Icon({
            iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
            shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            shadowSize: [41, 41]
          });

      var marker2 = L.marker(e.latlng, {icon: greenIcon}).bindPopup("Calculer les observables en ce point");

      marker2.on("add", function (event) {
            event.target.openPopup();
            });
            myLayerGroupForListPoint.addLayer(marker2);

            mutliPointInput.value =  mutliPointInput.value + "\n" + e.latlng.lng.toFixed(4) + "  " + e.latlng.lat.toFixed(4) ;
});

      //récupération des données de l'éllipsoid envoyé par djnago
const DataFromList = JSON.parse(document.getElementById('DataFromList').textContent);

var inputCalculLP = document.getElementById("inputCalculLP");
inputCalculLP.addEventListener("click", calculEnCoursLP);

function calculEnCoursLP(e){
      if (document.getElementById("mutliPointInput").value != ''){
            document.getElementById("CalculPoint").innerHTML = "Calcul en cours sur une liste de point...";
            document.getElementById("CalculPoint").style.color = "red";
            document.getElementById("CalculEmprise").innerHTML = "Calcul en cours sur une liste de point...";
            document.getElementById("CalculEmprise").style.color = "red";
            document.getElementById("CalculLP").innerHTML = "Calcul en cours sur une liste de point...";
            document.getElementById("CalculLP").style.color = "red";
      }
}

// console.log(DataFromList);

var marker;

var popup;

for (let [keyListePoint, valueListePoint] of Object.entries(DataFromList)){
      // console.log([valueListePoint[2].value, valueListePoint[1].value]);

      marker = L.marker([valueListePoint[1].value, valueListePoint[2].value]).bindPopup("Un point de la liste du plus recent calcul des obs");

      myLayerGroupForListPoint.addLayer(marker);

      latPlotly .push(valueListePoint[1].value);
      lngPlotly .push(valueListePoint[2].value);

};

mapListePoint.fitBounds(myLayerGroupForListPoint.getBounds())

observable_index = JSON.parse(document.getElementById('num_observable_name_champs').textContent);

var list_obs_multi_point = document.getElementsByClassName("list_obs_multi_point");
for (var i = 0; i < list_obs_multi_point.length; i++) {
      list_obs_multi_point[i].addEventListener("click", function(e) {
      currentListePoint = document.getElementsByClassName("activeListPoint");
  if (currentListePoint.length !=0){
      currentListePoint[0].className = currentListePoint[0].className.replace(" activeListPoint", "");
      }
      e.target.className += " activeListPoint";

      // console.log(this.innerText);

      myLayerGroupForListPoint.clearLayers();

      for (let [keyListePoint, valueListePoint] of Object.entries(DataFromList)){
            // console.log([valueListePoint[2].valueListePoint, valueListePoint[1].valueListePoint]);

            numListePoint = observable_index[e.target.innerText];
            // console.log(numListePoint);


            marker = L.marker([valueListePoint[1].value, valueListePoint[2].value]).bindPopup("" + valueListePoint[numListePoint].value.toFixed(3) + " " + valueListePoint[numListePoint].unit);

            marker.on("add", function (event) {
      event.target.openPopup();
   });

            myLayerGroupForListPoint.addLayer(marker);

            obsPlotly.push(valueListePoint[numListePoint].value);
      }

      showPlotlyMapListePoint();

      });
};


var plotlyListPointCB = document.getElementById("plotlyListPointCB");

plotlyListPointCB.addEventListener('change', showPlotlyMapListePoint);
document.getElementById("mapListePointPlotly").style.visibility = "hidden";

const average = arr => arr.reduce((a,b) => a + b, 0) / arr.length
meanLat = average(latPlotly);
meanLng = average(lngPlotly);


function selectionParDefautListePoint(){
      var currentListePoint = document.getElementsByClassName("activeListPoint");
      // console.log(currentListePoint);
      if(currentListePoint.length ==0){
    
            list_obs_multi_point[2].className += " activeListPoint";
      
      //     console.log(list_obs_multi_point[2].innerHTML);
      
          var num;
      
          for (let [key, value] of Object.entries(DataFromList)){
                // console.log([value[2].value, value[1].value]);
      
                num = 3;
      
                obsPlotly.push(value[num].value);
          }
      }
}



function showPlotlyMapListePoint(){
      
      if (plotlyListPointCB.checked){

            currentListePoint = document.getElementsByClassName("activeListPoint");
            // console.log(currentListePoint);

            if(obsPlotly ==0){
                  // console.log(obsPlotly);
                  if(currentListePoint.length !=0){
                    numListePoint = observable_index[currentListePoint[0].innerHTML];
        
                    for (var [keyListePoint, valueListePoint] of Object.entries(DataFromList)){
        
                      obsPlotly.push(valueListePoint[numListePoint].value);
                }
                  }
        
        
                }

                selectionParDefautListePoint();


      document.getElementById("mapListePointPlotly").style.visibility = "visible";
      
      var data = [{

            type:'scattermapbox',
          
            lat:latPlotly ,
          
            lon:lngPlotly,

            text: obsPlotly,
          
            mode:'markers',
          
            marker: {

            color: obsPlotly,
          
              size:14, 

              colorbar:{
                  thickness: 10,
                  titleside: 'right',
                  outlinecolor: 'rgba(68,68,68,0)',
                  ticks: 'outside',
                  ticklen: 3
                  // shoticksuffix: 'last',
                  // ticksuffix: 'inches',
                  // dtick: 0.1
                }
          
            },

            coloraxis: 'coloraxis'

            
          
            // text:['Montreal']
          
          }]
          
          
          var layout = {
          
            autosize: false,
          
            hovermode:'closest',

            width:  683,
            height: 405,

            coloraxis: {colorscale: "Viridis"},

            margin: {
                  r: 0,
                  t: 0,
                  b: 0,
                  l: 0,
                  pad: 0
                },
          
            mapbox: {
                  style:"satellite",
          
              bearing:0,
          
              center: {
          
                lat:meanLat,
          
                lon:meanLng
          
              },
          
              pitch:0,
          
              zoom:mapListePoint.getZoom() - 1
          
            },
          
          }
          
          
          Plotly.setPlotConfig({
            mapboxAccessToken: "pk.eyJ1IjoiZ3QyMDAiLCJhIjoiY2wwMTdha21xMDl3OTNjbzN4eGdscjRrZCJ9.7SIEP5kN0I57_6JL7Jvjrg"
          })
          
          
          Plotly.newPlot('mapListePointPlotly', data, layout); 

          obsPlotly = [];

      }

          else{
            document.getElementById("mapListePointPlotly").style.visibility = "hidden";
            obsPlotly = [];

          }

          
}

// // console.log(DataFromList);
// var observable_index = {"lat": 1, "lng":2, "Hauteur Geoportail/Etopo1": 3, "Total Gravitational Potential": 4, "Total Centrifugal Potential":5,  "Total Gravitational Potential":6, "Normal Gravitational Potential":7, "Normal Centrifugal Potential": 8, "Normal Gravity Potential":9, "Dist. Gravitational Potential":10,"Dist. Centrifugal Potential": 11, "Dist. Gravity Potential": 12, "Gravity Disturbance (abs(g)-abs(gamma))": 13,"Gravity Anomaly": 14,"Deflection N/S": 15, "Deflection E/W":16,"Total Gravity at h":17, "Normal Gravity at h computed":18, "Normal Gravity at h formula": 19,"Height Anomaly": 20,"Gravity Gradients T_xx": 21, "Gravity Gradients T_xy": 22, "Gravity Gradients T_xz": 23, "Gravity Gradients T_yy": 24, "Gravity Gradients T_yz": 25, "Gravity Gradients T_zz": 26};




