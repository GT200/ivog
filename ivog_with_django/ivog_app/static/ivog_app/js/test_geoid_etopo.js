//récupération des données de l'éllipsoid envoyé par djnago
const ellips_data = JSON.parse(document.getElementById('geoid_etopo_data').textContent);
//récuprération du div de l'éllipsoid
let ellipsoid = document.getElementById('geoid_etopo');

// donnée brute et graphique de l'ellipsoid
let data = [{
  x: ellips_data[0], y: ellips_data[1], z: ellips_data[2],
  surfacecolor  : ellips_data[3],
  type: 'scatter3d',
  colorscale : 'Blue',
  lighting: {ambient: 0.8}
  },
];

let layout = {
  // title: 'Ellipsoid ',
  title:{text:"Ellipsoid/geoid", font:{family:"Arial Black", color:"blue", size:16}, xanchor:"right"},
  autosize: false,
   width: 600,
   height: 550,
   margin: {
    l: 5,
    r: 5,
    b: 15,
    t: 25,
  },
  pad: {
    l: 5,
    r: 5,
    b: 15,
    t: 0,
  }
};

Plotly.react(ellipsoid, data, layout);



