var point = document.getElementById("point");
var listePoint = document.getElementById("listePoint");
var emprise = document.getElementById("emprise");
var catalog = document.getElementById("catalog");

var optionPoint = document.getElementById("optionPoint");
var optionListePoint = document.getElementById("optionListePoint");
var optionEmprise = document.getElementById("optionEmprise");
var optionCatalog = document.getElementById("optionCatalog");

point.addEventListener("click", info_affiche_point);
listePoint.addEventListener("click", info_affiche_ListePoint);
emprise.addEventListener("click", info_affiche_Emprise);

if (catalog != null){catalog.addEventListener("click", info_catalog);};


var btnActive = document.getElementsByClassName("active2");
var plotlyListPointCB = document.getElementById("plotlyListPointCB");
var plotlyListPointCB2 = document.getElementById("plotlyListPointCB2");
var plotlyListPointCB3 = document.getElementById("plotlyListPointCB3");

function changeActiveBtn(e){
  if (btnActive.length !=0){
    btnActive[0].className = btnActive[0].className.replace("active2", "");
    }
    e.className += " active2";
    console.log(e.innerText);
}
 
function info_affiche_point(){
  optionPoint.style.visibility = "visible";
  optionListePoint.style.visibility = "hidden";
  optionEmprise.style.visibility = "hidden";
  if (catalog != null){ optionCatalog.style.visibility = "hidden";};
  changeActiveBtn(this);

  plotlyListPointCB.checked = false;
  document.getElementById("mapListePointPlotly").style.visibility = "hidden"

  plotlyListPointCB2.checked = false;
  document.getElementById("mapEmprisePlotly").style.visibility = "hidden";

  plotlyListPointCB3.checked = false;
  document.getElementById("mapEmpriseGMT").style.visibility = "hidden";

  document.getElementById("mapEmprise").style.visibility = "hidden";

}

function info_affiche_ListePoint(){
  optionPoint.style.visibility = "hidden";
  optionListePoint.style.visibility = "visible";
  optionEmprise.style.visibility = "hidden";
  if (catalog != null){ optionCatalog.style.visibility = "hidden";};
  changeActiveBtn(this);
  plotlyListPointCB2.checked = false;
  document.getElementById("mapEmprisePlotly").style.visibility = "hidden";
  plotlyListPointCB3.checked = false;
  document.getElementById("mapEmpriseGMT").style.visibility = "hidden";

  document.getElementById("mapEmprise").style.visibility = "hidden";


}

function info_affiche_Emprise(){
    // console.log("mememememem");
  optionPoint.style.visibility = "hidden";
  optionListePoint.style.visibility = "hidden";
  optionEmprise.style.visibility = "visible";
  if (catalog != null){ optionCatalog.style.visibility = "hidden";};
  changeActiveBtn(this);
  plotlyListPointCB.checked = false;
  document.getElementById("mapListePointPlotly").style.visibility = "hidden";
  document.getElementById("mapEmprise").style.visibility = "visible";
  document.getElementById("mapEmpriseGMT").style.visibility = "hidden";
}

function info_catalog(){
    // console.log("mememememem");
    optionPoint.style.visibility = "hidden";
    optionListePoint.style.visibility = "hidden";
    optionEmprise.style.visibility = "hidden";
    if (catalog != null){ optionCatalog.style.visibility = "visible";};
    changeActiveBtn(this);
    plotlyListPointCB.checked = false;
    document.getElementById("mapListePointPlotly").style.visibility = "hidden";
    document.getElementById("mapEmprise").style.visibility = "hidden";
    document.getElementById("mapEmprisePlotly").style.visibility = "hidden";
    document.getElementById("mapEmpriseGMT").style.visibility = "hidden";
  }

const clickChoice = JSON.parse(document.getElementById('clickChoice').textContent);

console.log(clickChoice);

if (clickChoice[0] == 1){
    point.click();
}
if (clickChoice[1] == 1){
    listePoint.click();
}
if (clickChoice[2] == 1){
    emprise.click();
}
