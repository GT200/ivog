var comp_ell = document.getElementById("selectEllipsoidComp"); 
var img_comp_ell = document.getElementById("imgellipsComparer"); 

img_comp_ell.src =  "static/ivog_app/img/imgEllipsoid/" + comp_ell.value + ".png";

comp_ell.addEventListener("change", changeImg);

function changeImg(e){
    const select = e.target;
    const val= select.value;
    img_comp_ell.src =  "static/ivog_app/img/imgEllipsoid/" + val + ".png";
}