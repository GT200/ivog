//récupération des données de l'éllipsoid envoyé par djnago
const ellips_data = JSON.parse(document.getElementById('ellips_data').textContent);
//récuprération du div de l'éllipsoid
let ellipsoid = document.getElementById('ellips');

// donnée brute et graphique de l'ellipsoid
let data = [{
  x: ellips_data[0], y: ellips_data[1], z: ellips_data[2],
  surfacecolor  : ellips_data[3],
  type: 'surface',
  showscale: false,
  colorscale: 'Viridis',
  lighting: {ambient: 0.8}
  },
];

var max_x = Math.max.apply(Math, ellips_data[0].flat());
var max_y = Math.max.apply(Math, ellips_data[1].flat());
var max_z = Math.max.apply(Math, ellips_data[2].flat());
var max_range= Math.max(max_x, max_y, max_z);
console.log(max_range);

let layout = {
  // title: 'Ellipsoid ',
  // title:{text:"Ellipsoide", font:{family:"Arial Black", color:"red", size:16}, xanchor:"right"},
  width: 600,
  height: 400,
  margin: {
    r: 0,
    t: 0,
    b: 0,
    l: 0,
    pad: 0
  },

  scene:{

		aspectmode:'cube',

		domain:{row:0, column:0}, 

    xaxis: {
      showticklabels: false,
      showgrid: false,
      showline: false,
      
      range: [-max_range, max_range]
    },
  
    yaxis: {
      showticklabels: false,
      showgrid: false,
      showline: false,
      range: [-max_range, max_range]
    },
  
    zaxis: {
      showticklabels: false,
      showgrid: false,
      showline: false,
      range: [-max_range, max_range]
  
    }

	},
 
};

Plotly.react(ellipsoid, data, layout);



