var mapEmprise = L.map('mapEmprise').setView([51.505, -0.09], 13);
      L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox/satellite-streets-v9',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: 'pk.eyJ1IjoiYXhlbGxzYyIsImEiOiJja3V5MHJsOHMwemhsMnByZnJraHo3Y3NxIn0.-RSeYnibv28gdgJtmyLbMw'
      }).addTo(mapEmprise);

var mouseCoord3 = document.getElementById("mouseCoord3");
mapEmprise.on('mousemove', (e) => {
    // //console.log(e.latlng.lat);
    mouseCoord3.innerHTML = "Longitude:" + e.latlng.lng.toFixed(6) + ", Latitude:" + e.latlng.lat.toFixed(6);
    getEmrpise(e);
    });

var myLayerGroupForEmprise=L.featureGroup().addTo(mapEmprise);

// add Leaflet-Geoman controls with some options to the map  
mapEmprise.pm.addControls({  
  position: 'topleft',
  removalMode: false, 
  drawMarker: false,
  drawRectangle: false,
  drawCircle: false,  
  drawCircleMarker: false,
  drawPolyline: false,
  drawPolygon: false,
  cutPolygon: false,
  rotateMode: false
}); 

myLayerGroupForEmprise.setStyle({pmIgnore: false});
// layer.options.pmIgnore = false; // If the layer is a LayerGroup / FeatureGroup / GeoJSON this line is needed too

          //récupération des données de l'éllipsoid envoyé par djnago
const DataFromEmprise = JSON.parse(document.getElementById('DataFromEmprise').textContent);
const obsGMT = JSON.parse(document.getElementById('obsGMT').textContent);

var inputCalculEmprise = document.getElementById("inputCalculEmprise");
inputCalculEmprise.addEventListener("click", calculEnCoursEmprise);

function calculEnCoursEmprise(e){
      if (document.getElementById("lng1").value != ''){
            document.getElementById("CalculPoint").innerHTML = "Calcul en cours sur  une emprise...";
            document.getElementById("CalculPoint").style.color = "red";
            document.getElementById("CalculEmprise").innerHTML = "Calcul en cours sur  une emprise...";
            document.getElementById("CalculEmprise").style.color = "red";
            document.getElementById("CalculLP").innerHTML = "Calcul en cours sur  une emprise...";
            document.getElementById("CalculLP").style.color = "red";
      }
}


//console.log(DataFromEmprise);

var latPlotly2  = [];
var lngPlotly2  = [];
var obsPlotly2 = [];

var heatLeaflet = [];


for (let [key, value] of Object.entries(DataFromEmprise)){
    latPlotly2 .push(value[1].value);
    lngPlotly2 .push(value[2].value);
}

minLng = lngPlotly2.reduce((a, b) => { return Math.min(a, b) });
maxLng = lngPlotly2.reduce((a, b) => { return Math.max(a, b) });
minLat = latPlotly2.reduce((a, b) => { return Math.min(a, b) });
maxLat = latPlotly2.reduce((a, b) => { return Math.max(a, b) });

//console.log(minLng);
//console.log(maxLng);


// define rectangle geographical bounds
 var bounds = [[maxLat, maxLng], [minLat, minLng]];
// var bounds = [[minLng, maxLng], [minLat, maxLat]];

// create an orange rectangle

var rect = L.rectangle(bounds, {color: "#ff7800", weight: 1, pmIgnore: false }).bindPopup("Emprise le plus récent calculer sur ivog").addTo(mapEmprise);

function getCorners(layer) {
  const corners = layer.getBounds();

  const northwest = corners.getNorthWest();
  const northeast = corners.getNorthEast();
  const southeast = corners.getSouthEast();
  const southwest = corners.getSouthWest();

  return [northwest, northeast, southeast, southwest];
}


// enable Edit Mode
rect.pm.enable({
  allowEditing: true,
});

// zoom the map to the rectangle bounds
mapEmprise.fitBounds(bounds);
mapEmprise.setZoom(5);

var lng1 = document.getElementById("lng1");
var lng2 = document.getElementById("lng2");
var lat1 = document.getElementById("lat1");
var lat2 = document.getElementById("lat2");

//console.log(getCorners(rect));

lng1.value = getCorners(rect)[0].lng;
lng2.value = getCorners(rect)[1].lng;
lat1.value = getCorners(rect)[2].lat;
lat2.value = getCorners(rect)[0].lat;

function getEmrpise(e){
lng1.value = getCorners(rect)[0].lng;
lng2.value = getCorners(rect)[1].lng;
lat1.value = getCorners(rect)[2].lat;
lat2.value = getCorners(rect)[0].lat;
};

observable_index = JSON.parse(document.getElementById('num_observable_name_champs').textContent);

// //console.log("IIIIIIIIIINNNNNNNNNNNNNNNNNNDDDDDDDDDDDDDDDDDZZZZZZZX");
// //console.log(observable_index);

function changeImgGMT(img){
  if (plotlyListPointCB3.checked){
    choixGMT();

    var current = document.getElementsByClassName("activeEmrpise");

    document.getElementById("imgGMT").src = "static/"+img;
  }
  
 
}

var list_obs_emprise = document.getElementsByClassName("list_obs_emprise");


for (var i = 0; i < list_obs_emprise.length; i++) {
      list_obs_emprise[i].addEventListener("click", function(e) {
  var current = document.getElementsByClassName("activeEmrpise");
  if (current.length !=0){
      current[0].className = current[0].className.replace(" activeEmrpise", "");
      }
      this.className += " activeEmrpise";

      //console.log(this.innerHTML);

      myLayerGroupForEmprise.clearLayers();

      var num;

      for (let [key, value] of Object.entries(DataFromEmprise)){
            // //console.log([value[2].value, value[1].value]);

            num = observable_index[this.innerHTML];


            marker = L.marker([value[1].value, value[2].value]).bindPopup("" + value[num].value.toFixed(3) + " " + value[num].unit);

            heatLeaflet.push([value[2].value, value[1].value, value[num].value]);

            obsPlotly2.push(value[num].value);
      }

      // var heat = L.heatLayer(heatLeaflet, {radius: 5}).addTo(map);

      heatLeaflet = [];

      showPlotlyMap();

      // //console.log(obsGMT);
      obsGMT.forEach(element => {
        if (element.nameObs == this.innerHTML){
          // //console.log(element.nameObs);
          changeImgGMT(element.imgObs)
        }
        
      });


      });
};


var plotlyListPointCB2 = document.getElementById("plotlyListPointCB2");
var plotlyListPointCB3 = document.getElementById("plotlyListPointCB3");

// plotlyListPointCB3.addEventListener('change', choixCarteLeaflet);
// plotlyListPointCB3.addEventListener('change', choixCarteLeaflet);

plotlyListPointCB3.addEventListener('click', showGMTmap);
function showGMTmap(){
  // //console.log("CCCCCCCCCCCCCCCCCCCCC");
  if (plotlyListPointCB3.checked){
  
    var current = document.getElementsByClassName("activeEmrpise");

    // //console.log(current);

      if(current.length !=0){

        obsGMT.forEach(element => {
          if (element.nameObs == current[0].innerHTML){
            // //console.log(element.nameObs);
            changeImgGMT(element.imgObs)
          }
          
        });

    }

    else{

      obsGMT.forEach(element => {
        if (element.nameObs == "Hauteur DEM Geoportail/Etopo1"){
          //console.log(element.imgObs);
          changeImgGMT(element.imgObs)
        }
        
      });

      list_obs_emprise[2].className += " activeEmrpise";

    }

    document.getElementById("mapEmpriseGMT").style.visibility = "visible";
    
  }
  else{
    choixCarteLeaflet()
    document.getElementById("mapEmpriseGMT").style.visibility = "hidden"
  }
}

function choixGMT(){
  plotlyListPointCB3.checked = true;
  plotlyListPointCB2.checked = false;
  document.getElementById("mapEmprisePlotly").style.visibility = "hidden";
  document.getElementById("mapEmprise").style.visibility = "hidden";
  document.getElementById("mapEmpriseGMT").style.visibility = "visible"
}

function choixCarteChaleur(){
    plotlyListPointCB3.checked = false;
    document.getElementById("mapEmprisePlotly").style.visibility = "visible";
    document.getElementById("mapEmprise").style.visibility = "hidden";
    document.getElementById("mapEmpriseGMT").style.visibility = "hidden"
}

function choixCarteLeaflet(){
  plotlyListPointCB3.checked = false;
  plotlyListPointCB2.checked = false;
  document.getElementById("mapEmprisePlotly").style.visibility = "hidden";
  document.getElementById("mapEmprise").style.visibility = "visible";
  document.getElementById("mapEmpriseGMT").style.visibility = "hidden"
}

var classVignettes = document.getElementsByClassName("vignette");


for (var i = 0; i < classVignettes.length; i++) {
  classVignettes[i].addEventListener("click", showGmtFromVignette) 
  }
function showGmtFromVignette(e){
  //console.log(e.target.alt);
  document.getElementById("imgGMT").src = e.target.src;
  choixGMT()
  for (var i = 0; i < list_obs_emprise.length; i++) {
    if (list_obs_emprise[i].innerHTML == e.target.alt){
      var current = document.getElementsByClassName("activeEmrpise");
      if (current.length !=0){
          current[0].className = current[0].className.replace(" activeEmrpise", "");
          }
      list_obs_emprise[i].className += " activeEmrpise";
    }
  }

  
}


plotlyListPointCB2.addEventListener('click', showPlotlyMap);
 document.getElementById("mapEmprisePlotly").style.visibility = "hidden";

 const average2 = arr => arr.reduce((a,b) => a + b, 0) / arr.length
 meanLat2 = average2(latPlotly2);
 meanLng2 = average2(lngPlotly2);

 function selectionParDefaut(){
  var current = document.getElementsByClassName("activeEmrpise");
  if(current.length ==0){

    list_obs_emprise[2].className += " activeEmrpise";
  
      //console.log(list_obs_emprise[2].innerHTML);
  
      var num;
  
      for (let [key, value] of Object.entries(DataFromEmprise)){
            // //console.log([value[2].value, value[1].value]);
  
            num = 3;
  
            obsPlotly2.push(value[num].value);
      }
  }

 };



function showPlotlyMap(){

      if (plotlyListPointCB2.checked){

        choixCarteChaleur();

        var current = document.getElementsByClassName("activeEmrpise");

        if(obsPlotly2 ==0){
          if(current.length !=0){
            var num = observable_index[current[0].innerHTML];

            // //console.log(DataFromEmprise);

            for (let [key, value] of Object.entries(DataFromEmprise)){

              //console.log(value[num]);

              obsPlotly2.push(value[num].value);
        }
          }


        }

        selectionParDefaut()


      document.getElementById("mapEmprisePlotly").style.visibility = "visible";
      
      var data = [{

            type:'scattermapbox',
          
            lat:latPlotly2 ,
          
            lon:lngPlotly2,

            text: obsPlotly2,

            mode:'markers',
          
            marker: {

            color: obsPlotly2,
          
              size:20, 

              colorbar:{
                  thickness: 10,
                  titleside: 'right',
                  outlinecolor: 'rgba(68,68,68,0)',
                  ticks: 'outside',
                  ticklen: 3
                  // shoticksuffix: 'last',
                  // ticksuffix: 'inches',
                  // dtick: 0.1
                }
          
            },
            coloraxis: 'coloraxis'
          
            }]

          
          var layout = {
          
            autosize: false,
          
            hovermode:'closest',

            coloraxis: {colorscale: "Viridis"},

            width:  697,
            height: 405,

            margin: {
                  r: 0,
                  t: 0,
                  b: 0,
                  l: 0,
                  pad: 0
                },
          
            mapbox: {

              style:"satellite",
          
              bearing:0,
          
              center: {
          
                lat:meanLat2,
          
                lon:meanLng2
              },
          
              pitch:0,
          
              zoom:mapEmprise.getZoom() - 1
          
            },
          
          }
          
          
          Plotly.setPlotConfig({
            mapboxAccessToken: "pk.eyJ1IjoiZ3QyMDAiLCJhIjoiY2wwMTdha21xMDl3OTNjbzN4eGdscjRrZCJ9.7SIEP5kN0I57_6JL7Jvjrg"
          })
          
          
          Plotly.newPlot('mapEmprisePlotly', data, layout); 

          obsPlotly2 = [];

      }

          else{
            choixCarteLeaflet();
            document.getElementById("mapEmprisePlotly").style.visibility = "hidden";
            obsPlotly2 = [];

          }

          
}






// // //console.log(DataFromList);
// var observable_index = {"lat": 1, "lng":2, "Hauteur Geoportail/Etopo1": 3, "Total Gravitational Potential": 4, "Total Centrifugal Potential":5,  "Total Gravitational Potential":6, "Normal Gravitational Potential":7, "Normal Centrifugal Potential": 8, "Normal Gravity Potential":9, "Dist. Gravitational Potential":10,"Dist. Centrifugal Potential": 11, "Dist. Gravity Potential": 12, "Gravity Disturbance (abs(g)-abs(gamma))": 13,"Gravity Anomaly": 14,"Deflection N/S": 15, "Deflection E/W":16,"Total Gravity at h":17, "Normal Gravity at h computed":18, "Normal Gravity at h formula": 19,"Height Anomaly": 20,"Gravity Gradients T_xx": 21, "Gravity Gradients T_xy": 22, "Gravity Gradients T_xz": 23, "Gravity Gradients T_yy": 24, "Gravity Gradients T_yz": 25, "Gravity Gradients T_zz": 26};


