var Latitude = document.getElementById("Latitude");
var Longitude = document.getElementById("Longitude");

var mouseCoord = document.getElementById("mouseCoord");

var mapPoint = L.map('mapPoint').setView([51.505, -0.09], 13);
      L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox/satellite-streets-v9',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: 'pk.eyJ1IjoiYXhlbGxzYyIsImEiOiJja3V5MHJsOHMwemhsMnByZnJraHo3Y3NxIn0.-RSeYnibv28gdgJtmyLbMw'
      }).addTo(mapPoint);

//récupération des données de l'éllipsoid envoyé par djnago
const DataFromPoint = JSON.parse(document.getElementById('DataFromPoint').textContent);

// console.log(DataFromPoint);

var myLayerGroupFor=L.layerGroup().addTo(mapPoint);

var marker;
marker = L.marker([DataFromPoint[1].value, DataFromPoint[2].value]).bindPopup("Calcul le plus récent. Cliquer sur les observable pour constater leur valeur.");

marker.on("add", function (event) {
      event.target.openPopup();
   });
   myLayerGroupFor.addLayer(marker);


zoom = 5;

mapPoint.setView(marker.getLatLng() , zoom);

mapPoint.on('mousemove', (e) => {
      // console.log(e.latlng.lat);
      mouseCoord.innerText = "Longitude:" + e.latlng.lng.toFixed(8) + ", Latitude:" + e.latlng.lat.toFixed(8);
      }); 

      mapPoint.on('click', (e)=>{
      var marker2 = L.marker(e.latlng).bindPopup("Calculer les observables en ce point");

      marker2.on("add", function (event) {
            event.target.openPopup();
         });

         myLayerGroupFor.clearLayers();
         myLayerGroupFor.addLayer(marker2);

      Longitude.value = e.latlng.lng.toFixed(8);
      Latitude.value = e.latlng.lat.toFixed(8);
});

var list_obs_point = document.getElementsByClassName("list_obs_point");

var inputCalculPoint = document.getElementById("inputCalculPoint");
inputCalculPoint.addEventListener("click", calculEnCoursPoint);

function calculEnCoursPoint(e){
      if (document.getElementById("Longitude").value != ''){
            document.getElementById("CalculPoint").innerHTML = "Calcul en cours sur un point...";
            document.getElementById("CalculPoint").style.color = "red";
            document.getElementById("CalculEmprise").innerHTML = "Calcul en cours sur un point...";
            document.getElementById("CalculEmprise").style.color = "red";
            document.getElementById("CalculLP").innerHTML = "Calcul en cours sur un point...";
            document.getElementById("CalculLP").style.color = "red";
      }
}

for (var i = 0; i < list_obs_point.length; i++) {
      list_obs_point[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("activePoint");
  console.log(current);
  if (current.length !=0){
      console.log(current[0]);
  current[0].className = current[0].className.replace(" activePoint", "");
//   console.log(current[0]);
  }
  this.className += " activePoint";
   console.log(this.innerText);

  myLayerGroupFor.clearLayers();

      for (let [key, value] of Object.entries(DataFromPoint)){
            // console.log(value);
            if (value.title == this.innerText){

            marker = L.marker([DataFromPoint[1].value, DataFromPoint[2].value]).bindPopup(value.value.toFixed(8) + " " + value.unit);

            marker.on("add", function (event) {
                  event.target.openPopup();
               });
      
            myLayerGroupFor.addLayer(marker);

                  }
            }

      });
};

// console.log(DataFromPoint);



