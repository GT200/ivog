import os

import numpy as np
import pygmt
import xarray as xr

from ivog_app.managerDonneeAffichage.donneeDictionnaire import obs_global_champ, observable_index_alti

def createSaveGMTMapViewEmrpise(shapeGrille,observable_index,  numObservable = 19, geopotOutputData = "./calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/geopot_output_eigen_emprisePointObs", imgOutputFilename = "./static/ivog_app/img/observable_champs_potentiel_global/observable.png", ):

    cwd = os.getcwd() #to run sh files
    pwd = os.path.dirname(__file__) #replace present Wording directory
    os.chdir(pwd)


    geopotOutputData = np.genfromtxt(geopotOutputData, usecols= (0, 1, numObservable))

    lat = np.unique(geopotOutputData[:, 1])
    lon = np.unique( geopotOutputData[:, 0])

    # print(lon)
    # print("LONNNNNNNNNNNN", lon.shape)
    # print("LATTTTTTTTTTT", lat.shape)
    
    observable = geopotOutputData[:, 2].reshape(shapeGrille)
    # print("OBSSSSSS", observable.shape)

    dataLablled  = xr.DataArray(data = observable, dims = ["longitude", "latitude"], coords={ "latitude" :lat, "longitude" :lon})

    fig = pygmt.Figure()

    region = [
        lat.min() ,
        lat.max() ,
        lon.min() ,
        lon.max() ,
    ]

    pygmt.makecpt(cmap="jet", series=[observable.min(), observable.max()])

    # print("sqqfqf", numObservable + 1)
    

    fig.grdimage(
        grid=dataLablled,
        region=region,
        frame=True,
        cmap=True,
        # interpolation = "n",
        projection = "Q12c",
    )

    fig.coast(shorelines=1, transparency=45)

    fig.colorbar(frame=True)

    fig.savefig(imgOutputFilename)


def createAllObsImgEmrpise(shapeGrille, observable_index):
    for i in range(2, 26):
        if (i not in [7 - 1, 8 - 1, 10 - 1, 11 - 1,18 - 1, 21 - 1, 22 - 1, 23 - 1,  24 - 1, 25 - 1, 26 - 1]):
            createSaveGMTMapViewEmrpise(shapeGrille, observable_index, numObservable = i ,geopotOutputData = "./calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/geopot_output_eigen_emprisePointObs" ,imgOutputFilename = './static/ivog_app/img/obs_champs_potentiel/obsEmrprise' + str(i + 1) + '.png')


def createAllObsImgEmrpiseAlti(shapeGrille, observable_index):
    for i in range(0,10):
        createSaveGMTMapViewEmrpise(shapeGrille, observable_index, numObservable = i ,geopotOutputData = "./calcul/alti/altiData/altiEmprise" ,imgOutputFilename = './static/ivog_app/img/obsAlti/obsAlti' + str(i + 1) + '.png')


def createSaveGMTEllipsoid(lon, lat, observable, outputImgNane, title):

    cwd = os.getcwd() #où est initialiser le chemin du terminal 
    pwd = os.path.dirname(__file__) #où ce trouve ce script
    os.chdir(pwd)

    dataLablled  = xr.DataArray(data = observable, dims = ["longitude", "latitude"], coords={"longitude" :lon, "latitude" :lat})

    fig = pygmt.Figure()

    region = [
        lat.min() ,
        lat.max() ,
        lon.min() ,
        lon.max() ,
    ]

    pygmt.makecpt(cmap="jet", series=[observable.min(), observable.max()])
    
    fig.grdimage(
        grid=dataLablled,
        region=region,
        frame=True,
        cmap=True,
        # interpolation = "n",
        projection = "Q12c",
    )

    fig.coast(shorelines=1, transparency=45)

    fig.colorbar(frame='+l"Différence de distance géocentrique en mètre"')

    fig.savefig(outputImgNane)

    os.chdir(cwd)