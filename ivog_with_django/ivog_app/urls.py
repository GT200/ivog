from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('surfaceDeReference', views.surfaceDeReference, name='surfaceDeReference'),
    path('champs', views.champs, name='champs'),
    path('altitudes', views.altitudes, name='altitudes'),
    path('equipe', views.equipe, name='equipe'),
    path('contact', views.contact, name='contact'),
    path('download/<str:filename>', views.download,  name='download'),
]