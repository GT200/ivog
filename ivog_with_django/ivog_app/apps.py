from django.apps import AppConfig


class IvogAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ivog_app'
