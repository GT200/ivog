
import numpy as np
from ivog_app.calcul.champEtEllipsoid.autoGeopot import autoComputeChampsPotentiel
import os

from ivog_app.calcul.champEtEllipsoid.fileCreatorForGeopot import createInputFileFromCoordinateForGeopot

W0  = 62636853.4

def ellipsoid_ref(type_EllRef):
    
    if type_EllRef == "GRS80":
        # Ellipsoide GRG80
        a=6378137.0
        b= 6356752.3141
        omega=7292115*1e-11
        GM = 3986005*1e8
        
        gamma_e=9.7803267715
        gamma_p=9.8321863685
        f=1./298.257222101
        m=0.00344978600308
        
    if type_EllRef == "WGS84":
        # Ellipsoide WGS84
        a=6378137.0
        b= 6356752.3142
        omega=7292115*1e-11
        GM = 3986004.418*1e8
        
        f=1/298.257223563
        gamma_e=9.7803267715
        gamma_p=9.8321863685
        m=0.00344978600308 
    else:
        # Ellipsoide GRG80
        a=6378137.0
        b= 6356752.3141
        omega=7292115*1e-11
        GM = 3986005*1e8
        
        gamma_e=9.7803267715
        gamma_p=9.8321863685
        f=1./298.257222101
        m=0.00344978600308 
    
    EllRef_cst=[a,b,omega,GM]
    EllRef_csts=[f,m,gamma_e,gamma_p]

    return (EllRef_cst,EllRef_csts)

def calc_somigilana_const(EllRef_cst):
    # Calcul des constantes utiles pour le calcul de la pesanteur normale avec la formule de Somigliana

    if len(EllRef_cst) != 4:
        return 0
    
    a=EllRef_cst[0]
    b=EllRef_cst[1]
    omega=EllRef_cst[2]
    GM=EllRef_cst[3]
    
    m=omega**2*a**2*b/GM
    f=(a-b)/a
    ep2=(a**2-b**2)/b/b
    ep=ep2**0.5
    q0=1/2*(1+3/ep2)*np.atan(ep) - 3/2/ep
    q0p=3*(1+1/ep2)*(1-np.atan(ep)/ep)-1

    gammae = GM/(a*b)*(1-m-(m*ep*q0p)/(6*q0))
    gammap=GM/a/a*(1+m*ep*q0p/3/q0)
    
    EllRef_csts=[f,m,gammae,gammap]

    return (EllRef_csts)

def somigliana(*args):
    # Calcul de la pesanteur normale a une latitude phi (formule de Somigliana)
    # Input :
    # - phi : latitude en degre decimal
    # - h : hauteur au dessus de l'ellipsoide  si h n'est pas fourni, la fonction calcule la pesanteur normale sur l'ellipsoide.
    # Output :
    # - g : pesanteur normale en m.s^-2
    #print(args)
    #print(len(args))

    phi = args[0]
    d2r = np.pi / 180
    phi = phi*d2r
    
    [EllRef_cst,EllRef_csts]=ellipsoid_ref("GRS80")
    a=EllRef_cst[0]
    b=EllRef_cst[1]
    omega=EllRef_cst[2]
    GM=EllRef_cst[3]

    #(gamma_e,gamma_p,m,f)=calc_somigilana_const([a,b,omega,GM])
    f=EllRef_csts[0]
    m=EllRef_csts[1]
    gamma_e=EllRef_csts[2]
    gamma_p=EllRef_csts[3]

    # Formule de Somigliana
    num=a*gamma_e*(np.cos(phi)**2)+b*gamma_p*(np.sin(phi)**2)
    denom=(a*a*(np.cos(phi)**2)+b*b*(np.sin(phi)**2))**0.5
    g0 = num/denom

    if len(args)>1:
        # Pesanteur normale au dessus de l'ellipsoide
        h = args[1]
        g=g0*(1-2*(1+f+m-2*f*np.sin(phi)**2)*h/a + 3 * (h/a)**2)
    else:
        # Pesanteur normale sur l'ellipsoide
        g=g0

    return g


def createImputGeopotFileForAlt(lon, lat, h, caclulSur):
    """
    createur d'un fichier input geopot pour alti
    """
    createInputFileFromCoordinateForGeopot(lon, lat, h, "alti"+caclulSur)

def launchGeopotForAlti(caclulSur, resolution, modele):
    if (caclulSur == "Point"):
        autoComputeChampsPotentiel("altiPoint", resolution, modele)
    elif (caclulSur == "ListePoint"):
        autoComputeChampsPotentiel("altiListePoint", resolution, modele)
    else:
        autoComputeChampsPotentiel("altiEmprise")

def alti_normale(C,phi):
    # Calcul de l'altitude normale à partir de la cote geopotentielle et de la latitude
    # Input :
    # - C : cote geopotentielle en M
    # - phi : latitude en degre decimal
    # Output :
    # - H : altitude normale

    # phi en radian
    phir = phi * np.pi / 180
    
    [EllRef_cst,EllRef_csts]=ellipsoid_ref("GRS80")
    a=EllRef_cst[0]
    b=EllRef_cst[1]
    omega=EllRef_cst[2]
    GM=EllRef_cst[3]

    #(gamma_e,gamma_p,m,f)=calc_somigilana_const([a,b,omega,GM])
    m=EllRef_csts[0]
    f=EllRef_csts[1]
    gamma_e=EllRef_csts[2]
    gamma_p=EllRef_csts[3]
    
    gamma_0=somigliana(phi,0.0)

    #A =  1 + f + m - 2 * f * (sin(phi))^2
    #x = C / (a * gamma_0)
    #H =  (C/ gamma_0)*(1 + A * x + x^2)

    # Approximation standard précision cm
    print(C.shape, gamma_0.shape, phir.shape)
    H= (C/ gamma_0)*( 1+( 1+f+m-2*f*(np.sin(phir))**2 )*(C/(a*gamma_0)) + (C/(a*gamma_0))**2)
        
    # Iterative jusqu'à l'itération k=3 (sub-micrométrique)
    k=0
    kmax=3
    Hk=[]
    H0=C/gamma_0
    Hk.append(H0)
    while k < kmax:
        Hkp1=H0/( 1-( 1+f+m-2*f*(np.sin(phir))**2 )*(Hk[k]/a) + (Hk[k]/a)**2)
        Hk.append(Hkp1)
        k=k+1
    
    return (gamma_0,H,Hk)

def alti_ortho(C,g):
    # Calcul de l'altitude orthometrique à partir de la cote geopotentielle et le la pesanteur
    # La reduction est effectuee a l'aide du gradient de Poincare-Prey
    # Input :
    # - C : cote geopotentielle en M
    # - g : pesanteur en M
    # Output :
    # - H : altitude orthometrique

    # gradient de Poincare-Prey
    dgdh=-0.848e-6 # dans les masses
    #dgdh=-0.3084e-5 # hors des masses

    H0=C/g
    H = H0*(1 + 0.5*dgdh*H0/g)
    
    # Iterative jusqu'à l'itération k=3 (sub-micrométrique)
    k=0
    kmax=3
    Hk=[]
    Hk.append(H0)
    while k < kmax:
        Hkp1=H0/(1 - 0.5*dgdh*Hk[k]/g)
        Hk.append(Hkp1)
        k=k+1
    
    return (H, Hk)


def alti(long_lambda, lat_phi, h, caclulSur, resolution, modele):
    """
    calcul les différent type d'altitude
    """
    global W0
    createImputGeopotFileForAlt(long_lambda, lat_phi, h, caclulSur)
    
    launchGeopotForAlti(caclulSur, resolution, modele)

    cwd = os.getcwd() #où est initialiser le chemin du terminal 
    pwd = os.path.dirname(__file__) #où ce trouve ce script
    os.chdir(pwd)


    W = np.genfromtxt("../champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/geopot_output_eigen_alti" + caclulSur , usecols= (5)).reshape((-1, 1))
    g = np.genfromtxt("../champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/geopot_output_eigen_alti" + caclulSur , usecols= (16)).reshape((-1, 1))
    print(W)
    C = W0 - W
    
    gamma_0=somigliana(45.0)
    
    Hd=C/gamma_0


    Hn = alti_normale(C, lat_phi)[1]

    Ho = alti_ortho(C, g)[0]
    
    data_input = np.hstack(( lat_phi, long_lambda, h ,  W, Hd, Hn, Ho, Hd - Hn,  Ho - Hn, Hd - Ho))
    data_input = np.round(data_input, 6)

    np.savetxt( "./altiData/alti" + caclulSur, data_input, fmt='%f')
    print("file saved")
    os.chdir(cwd)

