version 1.0 du 18/03/2022
by glion
Projet PDI11 : IVOG

====================================================================================
Ne pas diffuser le code source de geopot, ni les autres sources fortran. Code privé.
====================================================================================


**** A réfléchir / faire ****
1) Comment récupérer des coords géographiques de points (lat/lon/h) publique ? (je ne peux pas fournir un DTM à pas fins de l'IGN...)
=> Etopo1 ? ou autre DTM ?
2) Si un utilisateur demande des points de grilles réguliers différents de l'échantillongae d'un modele etopo ou autre - comment interpoler les points ?
3) Idem pour des points particuliers. 

=> faudra coder une méthode de krigeage ou interpolation classique ?

4) Curseur lissage de la donnée : si on a une observable géodésique à 10km de résolution => on peu la filtrer à basse résolution avec un moyenne glissante ?
5) dans les OG pas de hauteur de géoide N. Elle n'est pas calculée dans l'option 7 de geopot mais option 8. Le calcule est différent.
Si j'ai le temps je modifierai le script pour le prise en charge de l'option 8. Mais dans l'option on peut calculer l'anomlie d'altiture si la surface de réf est le quasi-géoide. C'est déjà bien.
6) Il faudra surement modifier geopot pour les modeles de champ (l388).  Sont prise en charge uniquement : EIGEN-6C4, egm96/2008 et osu91a.


================================================================
INFOS SUR LE CODE GEOPOT + lanceur
================================================================


========================== DRIVER SH ===========================
Les codes tournent uniquement sur unix + shell bourne SH

>>> Le lanceur :
./driver_GlobalModel.sh coord_points degree_resolution cut_off [dodatabrut] : c'est le driver qui va appeler les scripts pour retirer ou pas les basses longueur d'onde du champ.

avec : 
coord_points : fichier de coord points geographiques (id/lon/lat/h/xxx)
degree_resolution : degre de dév en HS (doit etre inf au modele)
cut_off : degree retrait des grandes longueur d'onde 
[dodatabrut] : optionnel => 1 si forcage recalcul du model brut SINON 0 on garde le fichier existant


========================== DEPENDANCE SH ===========================

>>> Il a besoin de 2 fichiers de configurations :
- GPFM_set_pattern : le fichier directeur lu par le programme "geopot_glion.x"
- set_driver_GlobalModel : un script pour paramétrer le lanceur

>>> Dépendance du lanceur : 
./FM_rmBF.sh : calcul et retire les basses fréquences du champ
./lst4geopot.sh : mise en forme du fichier liste de point pour geopot
./Swap_lonlat.sh : permute les colonnes lat et lon si besoin
./geopot_FM.sh : lance geopot avec les bonnes infos


========================== PROGRAMMES FORTRAN ===========================

>>> Programmes utilisés dans les scripts :
- geopot_glion.x => génère les observables géodésiques (option 7)
- diff_data.x => différence de 2 fichiers
- create_filter.x => créé un filre passe pas ou haut de type Poisson
- Filter.x => génère les coffs avec filtre passe bas ou passe haut


========================== DATA ===========================

>>> Data input :
- eigen-6c4.gfc.form : modele de champ pre-formaté à générer dans Data (voir ReadMe)
- FceIGN_Auv_ptloc_weighted : fichier de points id/lon/lat/h/pondération

>>> Data intermédiaire : 
- eigen-6c4.gfc.ffbf : coef du modele de champ filtre
- Filtre_LP80 : filtre de poisson a convoluer avec les coeffs du modele de champ

>>> Data output :
- geopotFM_EIGEN6c4_1000_FceIGN_Auv_ptloc_weighted : données brutes non filtree
- geopotFM_EIGEN6c4_1000_FceIGN_Auv_ptloc_weighted.FBF80 : données avec coeff grande longueur d'onde jusque fréquence de coupure
- geopotFM_EIGEN6c4_1000_FceIGN_Auv_ptloc_weighted.rmBF80 : données sans grandes longueurs d'onde


========================== Header =============================
1  lat [deg]                    latitude geo
2  lon [deg]                    longitude geo
3  h [m]                        Hauteur ellipsoidale
4  V [m^2/s^2]                  Total  Gravitational Potential
5  \Phi [m^2/s^2]               Total  Centrifugal   Potential
6  W [m^2/s^2]                  Total  Gravity       Potential
7  \Psi [m^2/s^2]               Normal Gravitational Potential
8  Phi_c [m^2/s^2]              Normal Centrifugal   Potential
9  U [m^2/s^2]                  Normal Gravity       Potential
10 \deltaV [m^2/s^2]            Dist.  Gravitational Potential
11 \delta\Phi_c [m^2/s^2]       Dist.  Centrifugal   Potential
12 T [m^2/s^2]                  Dist.  Gravity       Potential
13 \deltag [mGals]              Gravity Disturbance (abs(g)-abs(gamma))
14 \Deltag [mGals]              Gravity Anomaly
15 \xi NS [arcsec]              Deflection N/S
16 \eta EW [arcsec]             Deflection E/W
17 g_h [m/sec**2]               Total  Gravity at h
18 \gamma chk [m/s^2]           Normal Gravity at h computed
19 \gamma h [m/s^2]             Normal Gravity at h formula
20 \zeta [m]                    Height Anomaly
21 T_{xx} [mGals/km]            Gravity Gradients
22 T_{xy} [mGals/km]
23 T_{xz} [mGals/km]
24 T_{yy} [mGals/km]
25 T_{yz} [mGals/km]
26 T_{zz} [mGals/km]

========================== Resultats ===========================
Les affichages sont données dans le dossier Test Auvergne


========================== EXECUTION ===========================

>>> exemple lancement :
./driver_GlobalModel.sh FceIGN_Auv_ptloc_weighted 1000 80 1


========================== A générer dans ivog ===========================

>>> Infos sur le fichier à générer : set_driver_GlobalModel 
FMname           EIGEN6c4                        [Ivog]    Nom du model de champ utilisé
FMfile           eigen-6c4.gfc                   [Ivog]    Nom du fichier model de champ utilisé
TMname           DVELL2012plusGRS80              [----]    Pas utilisé ici (OSF)
TMfile           dV_ELL_RET2012_plusGRS80.gfc    [----]    Pas utilisé ici (OSF)
nrcpus           4                               [Ivog]    Nombre de proc utilisé pour le calcul
nbloc            4                               [Ivog]    Nombre de découpage du fichier de points
Idfiltrage       1                               [----]    Type de filtrage 1 : passe bas - 2 : passe haut

Note : [Ivog] => valeur doivent etre modifies , sinon on ne change pas


>>> Infos sur le fichier à générer : GPFM_set_pattern pour "geopot_glion"
EIGEN-6C4                            [Ivog]   Nom du model de champ ?
y                                    [----]   Est-ce que le fichier est au bon format pour geopot ?
eigen-6c4.gfc.form                   [----]   Nom du fichier du model de champ formate ?
n                                    [----]   Mode de champ en non-tide system par defaut - voulez-vous changer ?
y                                    [----]   vitesse de rotation de la Terre correcte ?
7                                    [----]   Option de calcul du programme ?
splitfile4GP00.geopot                [Ivog]   Nom du fichier de coord_points (lat/lon/h) ?
output.splitfile4GP00.geopot.bak     [Ivog]   Fichier de sortie ?
1000                                 [Ivog]   Degree max du modele pour le dev en HS ?
3                                    [----]   Systeme de marrée pour les données de sortie ?
3                                    [----]   Type de parametre elliposide de réf pour le champ normal ? (3 = a,b,GM,omega)
6378137.00000                        [Ivog]   a
3986004.415d8                        [Ivog]   GM
6356752.314140                       [Ivog]   b
3                                    [----]   Systeme de marrée pour potentiel normal ?
2                                    [----]   Methode de calcul pour anomlie gravi ?

Note : [Ivog] => valeur doivent etre modifies , sinon on ne change pas
Tide free = non-tide ;)

