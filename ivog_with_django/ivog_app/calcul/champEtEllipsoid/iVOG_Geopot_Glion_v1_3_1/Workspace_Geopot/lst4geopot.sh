#!/bin/csh

## $1 : fichier entree lst au format : id/Lat/Lon/Height 

echo "Nombre d'arguments = " $#argv ":"
echo $argv[*]

set infile=$1

if ( ! -e $infile ) then
    echo Warning: $infile does not exist
    exit 0
endif

#set outfile=`echo $infile | cut -d. -f 1`
set outfile=$infile.geopot

if ( -e $outfile ) then
    echo "Warning: le fichier $outfile existe deja."

    #echo ">> Voulez-vous ecraser le fichier (y/n) ?"
    #set rep = $<

    #if ($rep == n) then
	#exit 1
    #endif
    rm $outfile
endif

echo "Mise en format Geopot du fichier -> $infile"
echo "Save file ->  $outfile"

## Affiche derniere ligne + supprime espace en debut ligne  
# + recupere nombre donnees
# set nbdata=`tail -1 $infile | sed -e "s/^ *//g" | cut -d\  -f 1`
set nbdata=`wc -l $infile | cut -d\  -f 1`

echo $nbdata > $outfile

## Sauvegarde au format : Lon/Lat/Height 
awk -F' ' '{printf $3 "\t" $2 "\t" $4 "\n"}' $infile >> $outfile

