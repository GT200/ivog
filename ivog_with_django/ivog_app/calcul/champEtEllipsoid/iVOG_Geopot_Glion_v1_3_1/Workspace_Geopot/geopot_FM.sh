#!/bin/bash

#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
# Script pour lancer geopot en parallel
#
# Args : infile degdevHS key_FBF
#
#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# Autheur : G. LION
# Modifié : 13/02/2015
# version : 1.0
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#

BINGEOPOT=../Fortran/bin/

echo "Nombre d'arguments = " $#
echo $@ # ou ${BASH_ARGV[*]}

if [ "$#" -lt "5" ]; then
    echo "Not enough arguments"
    exit 0
fi

# Fichier liste points id/Lat/Lon/h
infile=$1    # Z_42_47_4p5_9.MNT.lst.selN63

# Choix resolution 
degdevHS=$2

# Filtre cutoff basse frequences
key_FBF=$3
cutoff=$4
Idfiltrage=$5

# --- Par defaut ---
# Modele de champ
FieldModel=`sed -n '/FMfile/p' set_driver_GlobalModel | awk '{print $2}'`
nameFM=`sed -n '/FMname/p' set_driver_GlobalModel | awk '{print $2}'`
#FieldModel="egm2008.gfc"
#nameFM="EGM2008"
PathFieldModelDir="./Data"
PathFieldModel=$PathFieldModelDir/${FieldModel}

# Parametre calcul parallel
NR_CPUS=`sed -n '/nrcpus/p' set_driver_GlobalModel | awk '{print $2}'`         # nb de CPU
Nbloc=$NR_CPUS     # nb de split du fichier


###########################################################
###                 NE PAS MODIFIER
###########################################################

ici=$PWD
id=0

START=$(date +%s)

echo
echo "(Versions displayed with local utility \"version\")"
version >/dev/null 2>&1 && version "=o" $(_eat $0 $1)
set -o nounset

# generate background process
echo "" &
echo "PID du processus courant : $$"
echo "PID du processus lancé : $!"

###

nrwait() {
    local nrwait_my_arg
    if [[ -z $1 ]] ; then
	nrwait_my_arg=2
    else
	nrwait_my_arg=$1
    fi
    
    while [[ $(jobs -p | wc -l) -ge $nrwait_my_arg ]] ; do
	sleep 2;
	jobs > /dev/null
    done
  }

###

if [ ! -f $infile ]; then
    echo "$infile not found!"
    exit 0
fi

outputresult="geopotFM_${nameFM}_${degdevHS}_${infile}"
genericname="splitfile4GP"

if [ "$key_FBF" == "1" ]; then
if [ "$Idfiltrage" == "1" ]; then
    outputresult=$outputresult.FBF$cutoff
    genericname=${genericname}FBF
else
    outputresult=$outputresult.rmBF$cutoff
    genericname=${genericname}rmBF
fi
fi


#----------------------------------------------------------#
# Split du fichier location points pour traitement parallel
#----------------------------------------------------------#
echo "Decoupage du fichier ${infile} en ${Nbloc} fichiers :"
split -n l/${Nbloc} -d ${infile} ${genericname}
listsplitfile=`ls ${genericname}* | grep -v '\.'`
echo $listsplitfile


#----------------------------------------------------------#
# Mise en format pour Geopot
#----------------------------------------------------------#
echo "Mise en format <geopot> ..."

for num in $listsplitfile
do

echo "Traitement de $num ..."
./lst4geopot.sh ${num}

done

tmpv=`ls ${genericname}* | grep -v '\.'`
rm $tmpv


#----------------------------------------------------------#
# Extraction info header modele coeffs HS
#----------------------------------------------------------#
echo "Extraction info header modele coeffs HS ..."

if [ ! -f $PathFieldModel ]; then
    echo "$PathFieldModel not found!"
    exit 0
fi

listsplitfile=`ls ${genericname}*.geopot`

echo $listsplitfile

# sed -n '{/begin_of_head/,/end_of_head/p}' $PathFieldModel > tmp_hdr 
sed -n '{1,/end_of_head/p}' $PathFieldModel > tmp_hdr # car EGM ne contient pas de begin...

modelname=`sed -n '/modelname/p' tmp_hdr | awk '{print $2}'`
egc=`sed -n '/earth_gravity_constant/p' tmp_hdr | awk '{print $2}'`
radius=`sed -n '/radius/p' tmp_hdr | awk '{print $2}'`
masdeg=`sed -n '/max_degree/p' tmp_hdr | awk '{print $2}'`
err=`sed -n '/errors/p' tmp_hdr | awk '{print $2}'`
norm=`sed -n '/norm/p' tmp_hdr | awk '{print $2}'`
tide=`sed -n '/tide_system/p' tmp_hdr | awk '{print $2}'`


if [ ! -f ${FieldModel}.form ]; then
    echo "${FieldModel}.form not found!"
    echo "Creation lien symbolique ${PathFieldModel} vers ./${FieldModel}.form"
    ln -s ${PathFieldModel}.form ${FieldModel}.form
fi


#----------------------------------------------------------#
# Filtre basses frequences
#----------------------------------------------------------#

echo "Creation filtre ..."

if [ "$Idfiltrage" -eq 1 ]; then
FilterHP="Filtre_LP${cutoff}"
else
FilterHP="Filtre_HP${cutoff}"
fi

$BINGEOPOT/./create_filterHS.x <<EOF
2
$cutoff
$Idfiltrage
$degdevHS
$FilterHP
EOF


#----------------------------------------------------------#
# Filtrage des basses frequences
#----------------------------------------------------------#
if [ "$key_FBF" = 0 ]; then
   FMF=${FieldModel}.form
else
echo "Filtre basses frequences des coeffs HS ..."
FMF=${FieldModel}.ffbf
$BINGEOPOT/./FilterHS.x <<EOF
${FieldModel}.form
$FilterHP
${FMF}
EOF
fi


#----------------------------------------------------------#
# Geopot
#----------------------------------------------------------#
echo "Launch GEOPOT Multithreading ..."

for num in $listsplitfile
do
(

echo $num

echo "++++++++++++"
echo " "
echo "Traitement fichier $num"
echo "PID proc. courant : $$"
echo "PID proc. lancé : $!"

namein=${num}
nameout=output.${num}

geopotnameset=GPset_$num

cat GPFM_set_pattern | sed -e "s/#AA#/$modelname/;s/#BA#/$FMF/;s/#CA#/$namein/;s/#DA#/$nameout/;s/#DB#/$degdevHS/;s/#EA#/$radius/;s/#FA#/$egc/" > $geopotnameset

#nohup geopot < $geopotnameset > tmp_screen/screen.$num
$BINGEOPOT/./geopot_glion.x < $geopotnameset

) & 
nrwait $NR_CPUS
done

echo "" &
wait

echo "Creation fichier final $outputresult ..."

END=$(date +%s)
DIFF=$(( $END - $START ))

echo "Total CPU time : $DIFF sec"

cat `ls output.${genericname}* | tr ' ' '\n' | sort -n -t " " -k1.2 | tr '\n' ' '` > $outputresult

echo "Clean up ..."
\rm *${genericname}*
