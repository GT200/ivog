#!/bin/bash

# ex : ./addcolfromfile.sh FceIGN_Auv_ptloc_weighted Resultats/geopot_EIGEN6c4_2000_FceIGN_Auv_ptloc_weighted.rmtopoDVELL2012plusGRS80FBF80.swp

echo "Nombre d'arguments = " $#
echo $@ # ou ${BASH_ARGV[*]}

if [ "$#" -lt "2" ]; then
    echo "Not enough arguments"
    exit 0
fi

file1=$1
file2=$2

echo $file1
echo $file2

if [ ! -f $file1 ]; then
    echo "$file1 not found!"
    exit 0
fi

if [ ! -f $file2 ]; then
    echo "$file2 not found!"
    exit 0
fi

awk 'NR==FNR{a[NR]=$5;next}{print $0,a[FNR]}' $file1 $file2 > "geopot_Auv_ptloc_weighed"
