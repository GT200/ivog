#!/bin/bash

#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# Autheur : G. LION
# Modifié : 15/03/2022
# version : 1.1 pour iVOG
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#


# Modifier set_driver_GlobalModel
#
# ex appel -> ./driver_GlobalModel.sh FceIGN_Auv_ptloc_weighted 2000 150 1
#
# Penser à linker : 
# - Modele de champ
# ln -s /home/glion/Postdoc/chronogeo/Workspace/DATA/FieldModel/eigen-6c4.gfc.form eigen-6c4.gfc.form


###########################################################
###                      MODIFIABLE
###########################################################

echo ""
echo "*** [DEB] >> Construction du modele de champ de  synthese ***"

if [ "$#" -lt 3 ]; then
    echo "Not enough arguments"
    exit 0
fi

locpt=$1 #Z_42_47_4.5_9.SE35.lst
degdevHS=$2 #2000
cutoff=$3 #20

if [ "$#" -eq 3 ]; then
    dodatabrut=1 #0
else
    dodatabrut=$4 #0
fi

###########################################################
###                 NE PAS MODIFIER
###########################################################

if [ ! -f $locpt ]; then
    echo "$locpt not found!"
    exit 0
fi

if [ "$degdevHS" -lt 0 ] || [ "$degdevHS" -gt 2190 ]; then
    echo "Degre dev HS $degdevHS incorrect !"
    exit 0
fi

if [ "$cutoff" -lt 0 ] || [ "$cutoff" -gt 2190 ]; then
    echo "Degre coupure basse frequence $cutoff incorrect !"
    exit 0
fi

echo ""
echo "Settings :"
echo "Name file location points               = " $locpt
echo "Degre/resolution du model               = " $degdevHS
echo "Degre retrait basses frequences         = " $cutoff
echo "Recalculer data bruts (si non presents) = " $dodatabrut

./FM_rmBF.sh $locpt $degdevHS $cutoff $dodatabrut

#./TM_rmBF.sh $locpt $degdevHS $cutoff $dodatabrut

#./FMminustopo.sh $locpt $degdevHS $cutoff


echo ""
echo "*** [FIN] >> Construction du modele de champ de synthese ***"
