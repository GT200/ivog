#!/bin/bash

#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
# FM_rmBF : Script "semi-auto" pour retrait basses frequences d'un modele de 
# champ de gravite 
#
# Settings : 
# locpt=$1    -> Fichier liste points id/Lat/Lon/h (ex: Z_42_47_4.5_9.SE35.lst)
# degdevHS=$2 -> Degre dev HS du model champ (ex: 2000)
# cutoff=$3   -> Degre filtrage basse frequences (ex: 20)
#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# Autheur : G. LION
# Modifié : 25/05/2015
# version : 1.0
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#

BINGEOPOT=../Fortran/bin/

###########################################################
###                      MODIFIABLE
###########################################################

echo ""
echo "*** [DEB] Traitement modele global de champ ***"

if [ "$#" -lt 3 ]; then
    echo "Not enough arguments"
    exit 0
fi

locpt=$1 #Z_42_47_4.5_9.SE35.lst
degdevHS=$2 #2000
cutoff=$3 #20

if [ "$#" -eq 3 ]; then
    dodatabrut=1 #0
else
    dodatabrut=$4 #0
fi

# Prefixe nom generique
# nameFM="EIGEN6c4"
nameFM=`sed -n '/FMname/p' set_driver_GlobalModel | awk '{print $2}'`
Idfiltrage=`sed -n '/Idfiltrage/p' set_driver_GlobalModel | awk '{print $2}'`

gen=geopotFM_$nameFM

###########################################################
###                 NE PAS MODIFIER
###########################################################

if [ ! -f $locpt ]; then
    echo "$locpt not found!"
    exit 0
fi

if [ "$degdevHS" -lt 0 ] || [ "$degdevHS" -gt 2190 ]; then
    echo "Degre dev HS $degdevHS incorrect !"
    exit 0
fi

if [ "$cutoff" -lt 0 ] || [ "$cutoff" -gt 2190 ]; then
    echo "Degre coupure basse frequence $cutoff incorrect !"
    exit 0
fi

if [ "$Idfiltrage" -lt 1 ] || [ "$Idfiltrage" -gt 2 ]; then
    echo "Type de filtrage incorrcet"
    exit 0
fi

echo ""
echo "Settings :"
echo "Name file location points       = " $locpt
echo "Degre/resolution du model       = " $degdevHS
echo "Degre retrait basses frequences = " $cutoff


if [ "$Idfiltrage" == "1" ]; then
    echo "Using LP filter"
else
    echo "Using HP filter"
fi

output1=${gen}_${degdevHS}_${locpt}
output2=${output1}.FBF${cutoff}
outputfin=${output1}.rmBF${cutoff}


if [ ! -f $output1 ] || [ "$dodatabrut" -eq 1 ]; then
echo ""
echo "Geopot + modele global SANS filtrage des basses frequences ..."
./geopot_FM.sh $locpt $degdevHS 0 $cutoff 0
fi

if [ "$cutoff" != 0 ]; then
echo "" 
echo "Geopot + modele global AVEC filtrage des basses frequences ..."
if [ "$Idfiltrage" = 1 ]; then
./geopot_FM.sh $locpt $degdevHS 1 $cutoff $Idfiltrage
echo ""
echo "Retrait des basses frequences ..."
$BINGEOPOT/./diff_data.x << EOF
$output1
$output2
4
26
1
$outputfin
EOF
else
./geopot_FM.sh $locpt $degdevHS 1 $cutoff $Idfiltrage
fi
fi

echo "*** [FIN] Traitement modele global de champ ***"
