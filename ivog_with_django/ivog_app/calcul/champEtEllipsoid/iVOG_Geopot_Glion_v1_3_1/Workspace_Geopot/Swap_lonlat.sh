#!/bin/bash

echo "Nombre d'arguments = " $#
echo $@ # ou ${BASH_ARGV[*]}

if [ "$#" -lt "1" ]; then
    echo "Not enough arguments"
    exit 0
fi

infile=$1

if [ ! -f $infile ]; then
    echo "$infile not found!"
    exit 0
fi

echo "Permute colone 1 et 2"
cat ${infile} | awk '{ t=$1; $1=$2; $2=t; print; }' > $infile.swp
# echo "Ordonne du min au max selon 2"
# cat ${infile}.swp | sort -t" "  -k2,2 > $infile.tri


#\rm ${infile}.swp
