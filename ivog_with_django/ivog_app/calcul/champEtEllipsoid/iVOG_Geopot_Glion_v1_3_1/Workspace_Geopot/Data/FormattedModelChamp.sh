#!/bin/csh

echo "Nombre d'arguments = " $#argv ":"
echo $argv[*]

set infile=$1
set suffix=".form"

if ( ! -e $infile ) then
   echo Warning: $infile does not exist
   exit 0
endif

if ( -e $infile$suffix) then
   echo Warning: $infile$suffix exists

   echo " "
   echo ">> Voulez-vous l'ecraser (y/n) ?"
   set rep=$<

   if ($rep == n) then
      exit 1
   endif
    
endif

# Suppression du header + extraction donnee
sed -e '1,/end_of_head/d' -e '/end_of_head/d' $infile | awk -F' ' '{printf $2 "\t" $3 "\t" $4 "\t" $5 "\n"}' > $infile.tmp

# Coeffs du champs Cnm classés en balayant m pour n fixé
sort -k1,1n -k2,2n $infile.tmp > $infile.form

\rm  $infile.tmp
