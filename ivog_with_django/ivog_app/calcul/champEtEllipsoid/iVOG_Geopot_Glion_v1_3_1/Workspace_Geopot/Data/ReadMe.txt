Récupérer un modelede champ sur ICGEM : eigen-6C4, egm2008, egm96 par ex.

Pour mettre au format avec :
./FormattedModelChamp.sh eigen-6c4.gfc

Un fichier :
eigen-6c4.gfc.from 

sera généré sans header et un bon format de colones.

Créer un lien symbolique vers le répertoire de travail :
ln -s rep_path_to_FM/eigen-6c4.gfc.form eigen-6c4.gfc.form
