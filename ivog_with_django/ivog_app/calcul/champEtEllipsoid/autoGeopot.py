import subprocess
import os 
#chmod -R 777  ./iVOG_Geopot_Glion_v1_3.1
#sudo apt install libgfortran5

def autoComputeChampsPotentiel(inputGeopotFilename = "test", resolution = "360", modele = "eigen6c4"):
    """
    Execute automatiquement le software bash geopot pour le calcul des observables geodesique par rapport à ce projet 

    inputGeopotFilename: nom du ficher de jeu de données est se trouvant dans le dossier ./iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot
    """

    # Get the current and present working directory
    cwd = os.getcwd() #où est initialiser le chemin du terminal 
    pwd = os.path.dirname(__file__) #où ce trouve ce script
    #les 2 chemin ne sont pas forcement le même

    # Change the current working directory wherer this script is 
    os.chdir(pwd + "/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot")
    #run geopot
    if (modele == "eigen6c4"):
        #process = subprocess.Popen(["./driver_GlobalModel.sh", inputGeopotFilename, "1000", "80", "1"])
        process = subprocess.Popen(["./driver_GlobalModel.sh", inputGeopotFilename, resolution, "80", "1"],
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()

        os.rename("geopotFM_EIGEN6c4_" + str(resolution) + "_"+ inputGeopotFilename, "geopot_output_eigen_" + inputGeopotFilename)

        os.remove("geopotFM_EIGEN6c4_" + str(resolution) + "_"+ inputGeopotFilename + ".FBF80") 
        os.remove("geopotFM_EIGEN6c4_" + str(resolution) + "_"+ inputGeopotFilename + ".rmBF80") 


    #reset Current working directory to its right place
    os.chdir(cwd)
#autoComputeChampsPotentiel()










