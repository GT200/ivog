# import module
from geopy.geocoders import Nominatim

def countryIsoCode(Longitude = 1.457426000000000 , Latitude =45.64342200000000 ):
    """
    récupère le code ISO d'un pays en du coordonnées en entrée
    """
    # initialize Nominatim API 
    geolocator = Nominatim(user_agent="geoapiExercises")

    Longitude = str(Longitude)
    Latitude =str(Latitude) 


    location = geolocator.reverse(Latitude+","+Longitude)
    
    # Display
    print(location)

    address = location.raw['address']
    print(address['country_code'])
    return(address['country_code'].upper())