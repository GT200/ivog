
import numpy as np
import pyproj
from ivog_app.calcul.champEtEllipsoid.ellipsoid import createEllipsoidGlobalGrid

from ivog_app.createSaveGMTView import createSaveGMTEllipsoid 


def getElipsoidNames():
    # dico_ellips = pyproj.get_ellps_map()
    # keys = dico_ellips.keys()
    # return(sorted(list(keys)))
    return(['GRS67', 'GRS80', 'WGS60', 'WGS66', 'WGS72', 'WGS84', 'clrk66', 'clrk80', 'clrk80ign','sphere'])

##calcul la différence entre les c (le modele choisi en post et celui de référence) après après
##fonction qui prend en argument le modele ellips choisi pour trouver ses axes a b et c
##si on connait a et b de ce dico, c'est direct a b et c ok
##sinon b=none donc on a la deuxieme equation pour trouver le demi petit axe et ainsi a b et c ok
##rf correspond à l'aplatissement
#or f = (a-b)/a = 1/rf
#donc b = a(1-f) = a(1-(1/rf))

def calcul_diff_ellips(modele_choisi):

    dico_ellips = pyproj.get_ellps_map()
    keys = dico_ellips.keys()
    c_ellips = 0
    list_keys = []

    for key in keys:
        list_keys.append(key)

    listOfValues = list(dico_ellips.values())
    #print(listOfValues[0].get("a")) 
    for i in range(0,len(list_keys)):
        if list_keys[i]==modele_choisi:
            #print(i)
            #print(listOfValues[i])
            ellips_corr = listOfValues[i]
            #print(ellips_corr)
            a_ellips = ellips_corr.get("a")
            b_ellips = a_ellips
            if ellips_corr.get("b") :
                c_ellips = ellips_corr.get("b")
            else:
                #print("none")
                rf = ellips_corr.get("rf")
                f = 1/rf
                c_ellips = a_ellips*(1-f)
                #print(c_ellips)
        else:
            continue
    return a_ellips, c_ellips


def compare2eps(nomRef):
    a1, b1 = calcul_diff_ellips(nomRef)
    (x1, y1, z1, lon1, lat1) = createEllipsoidGlobalGrid(a1, b1)
    lon1 = np.squeeze(lon1[0:1, :])
    lat1 =  np.squeeze(lat1[:, 0:1])
    noms = getElipsoidNames()
    # print(lat1)

    for nom in noms: 
        if (nom != nomRef):
            a2, b2 = calcul_diff_ellips(nom)
            (x2, y2, z2) = createEllipsoidGlobalGrid(a2, b2)[:3]
            obs = np.sqrt((z1 - z2)**2 + (y1 - y2)**2 + (x1 - x2)**2)
            createSaveGMTEllipsoid( lat1, lon1, obs, './static/ivog_app/img/imgEllipsoid/' + nom + '.png', nomRef + " vs "  + nom)
            # plt.imsave(, , cmap='RdBu')

