from requests import get
from pandas import json_normalize



def get_elevation_from_geoportail( long = None, lat = None):
    '''
        script for returning elevation in m from lat, long
    '''
    if lat is None or long is None: return None
    #https://www.freemaptools.com/elevation-finder.htm
    #query = ('https://api.open-elevation.com/api/v1/lookup'f'?locations={lat},{long}')
    #https://www.opentopodata.org/datasets/etopo1/
    query = (' https://wxs.ign.fr/calcul/alti/rest/elevation.json?'f'lon={long}&lat={lat}&zonly=true')

    # Request with a timeout for slow responses
    # print(query)
    r = get(query, timeout = 20)

    # # Only get the json response in case of 200 or 201
    if r.status_code == 200 or r.status_code == 201:
        elevation = r.json()['elevations'][0]
    else: 
         elevation = None
    return elevation

# print(get_elevation_from_geoportail( long = 0.2367, lat = 48))