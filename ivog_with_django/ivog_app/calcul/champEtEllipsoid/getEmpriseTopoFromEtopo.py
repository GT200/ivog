import numpy as np
import netCDF4

def getEmpriseDataTopo(lng1 = 4,lng2 = 7,lat1 = 46, lat2 = 48, echantillonage = 5):
    """

    ehantillonage: intervalle d'échantillonage par rapport à la résolution initial de etopo
    Ici, on prendra des valeur tous les unitié de 10 dans la grille topo
    
    """
    #https://www.ngdc.noaa.gov/thredds/catalog.html
    # open a local NetCDF file or remote OPeNDAP URL
    # url = 'http://www.ngdc.noaa.gov/thredds/dodsC/global/ETOPO1_Bed_g_gmt4.nc'
    # nc = netCDF4.Dataset(url)
    
    url = 'http://www.ngdc.noaa.gov/thredds/dodsC/global/ETOPO1_Bed_g_gmt4.nc'
    nc = netCDF4.Dataset(url)
    
    condition_lat  = np.logical_and(nc.variables['lat'][:]>lat1  ,nc.variables['lat'][:]<lat2)
    condition_lon  = np.logical_and(nc.variables['lon'][:]>lng1  ,nc.variables['lon'][:]<lng2)
    
    index_lat = np.where(condition_lat)[0]
    index_lon = np.where(condition_lon)[0]
    
    
    lat_emprise = nc.variables['lat'][:][condition_lat] #résolution 10km par rapport à etopo1 (1km)
    lon_emprise = nc.variables['lon'][:][condition_lon]  #résolution 10km par rapport à etopo1

    lat_emprise = lat_emprise[::echantillonage].reshape((-1, 1))
    lon_emprise = lon_emprise[::echantillonage].reshape((1, - 1))

    #API inversant lat et lon?
    # print(lat_emprise)
    
    lon, lat = np.meshgrid(lon_emprise , lat_emprise )
    
    z_emprise = nc.variables['z'][index_lat, index_lon]
    z_emprise = z_emprise[::echantillonage, ::echantillonage]

    # print("ETOPOEMP", lat.reshape((-1, 1)))

    return(lat.reshape((-1, 1)), lon.reshape((-1, 1)),  z_emprise.reshape((-1, 1)), z_emprise.shape)
