# import xarray as xr
# from numba import njit
# import pyximport
# pyximport.install()
import numpy as np
#import matplotlib.pyplot as plt

import netCDF4
#source: https://towardsdatascience.com/create-interactive-globe-earthquake-plot-in-python-b0b52b646f27

def getEarthTopo(ehantillonage = 1):
    """

    ehantillonage: intervalle d'échantillonage par rapport à la résolution initial de etopo
    Ici, on prendra des valeur tous les unitié de 10 dans la grille topo
    
    """
    #https://www.ngdc.noaa.gov/thredds/catalog.html
    # open a local NetCDF file or remote OPeNDAP URL
    url = 'http://www.ngdc.noaa.gov/thredds/dodsC/global/ETOPO1_Bed_g_gmt4.nc'
    nc = netCDF4.Dataset(url)

    # examine the variables
    # print(nc.geospatial_lon_resolution)
    # print(nc.geospatial_lat_resolution)
    geospatial_lon_resolution = nc.geospatial_lon_resolution
    geospatial_lat_resolution = nc.geospatial_lat_resolution
    geospatial_lat_min = nc.geospatial_lat_min
    geospatial_lat_max = nc.geospatial_lat_max
    geospatial_lon_min = nc.geospatial_lon_min
    geospatial_lon_max = nc.geospatial_lon_max

    # print (nc.variables['lat'].shape)
    # print (nc.variables['lon'].shape)
    # print (nc.variables['z'].shape)

    
    # print(nc.variables['z'][::ehantillonage,::ehantillonage].shape)
    # print(nc.variables['lat'][::ehantillonage].shape)
    # print(nc.variables['lon'][::ehantillonage].shape)

    topo = (nc.variables['z'][::ehantillonage,::ehantillonage])
    lon_input = (nc.variables['lat'][::ehantillonage])
    lat_input = (nc.variables['lon'][::ehantillonage])
    lon, lat = np.meshgrid(lon_input, lat_input, copy = False)

    #print(lon, lat)

    rad = np.pi/180
    demi_grand__axe, demi_petite_axe= 6378249.2, 6356515 #clarke ign a = 6378249.2m, b 6356515m
    a, b, c = demi_grand__axe, demi_grand__axe, demi_petite_axe
    xs = a * np.cos(lon * rad )*np.cos(lat * rad)
    ys = b * np.sin(lon * rad)*np.cos(lat * rad)
    zs = c * np.sin(lat * rad)

   
    # plt.figure(figsize=(10,10))
    # plt.imshow(topo,origin='lower') 
    # plt.title(nc.title)
    # plt.savefig('image.png', bbox_inches=0)
    # plt.show()

    return( lon, lat, topo, xs, ys, zs, lon_input, lat_input)

# topo = getEarthTopo()[2].shape
# print(topo)

from requests import get
from pandas import json_normalize



def get_elevation( long = None, lat = None):
    '''
        script for returning elevation in m from lat, long
    '''
    if lat is None or long is None: return None
    #https://www.freemaptools.com/elevation-finder.htm
    #query = ('https://api.open-elevation.com/api/v1/lookup'f'?locations={lat},{long}')
    #https://www.opentopodata.org/datasets/etopo1/
    query = ('https://api.opentopodata.org/v1/etopo1?'f'locations={lat},{long}')
    
    # Request with a timeout for slow responses
    r = get(query, timeout = 20)

    # Only get the json response in case of 200 or 201
    if r.status_code == 200 or r.status_code == 201:
        elevation = json_normalize(r.json(), 'results')['elevation'].values[0]
    else: 
        elevation = None
    return elevation

# lon =  str(1.457426000000000)    
# lat = str(45.64342200000000)   
# print(get_elevation(lon, lat ))

