import numpy as np
import os 

def createInputFileFromCoordinateForGeopot(lon, lat, h, filenamme="test"):
    """
    Description: créer un fichier au format input de geopot  
    param lon, lat, h: 1 column, n row numpy arra
    """
    id = np.arange(1, lon.shape[0] + 1, 1, dtype=int).reshape(lon.shape)
    # #print(id.shape)
    # # np.set_printoptions(suppress=True
    # # np.set_printoptions(precision=2)

    data_input = np.hstack((id, lon, lat, h ))
    data_input = np.round(data_input, 3)

    pwd = os.path.dirname(__file__) #où ce trouve ce script

    np.savetxt(pwd + "/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/" + filenamme, data_input, fmt='%f')
    print("file input geopot created and saved")
