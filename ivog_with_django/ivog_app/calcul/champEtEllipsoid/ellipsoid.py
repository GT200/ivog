
import numpy as np
import os

def createEllipsoidGlobalGrid(demi_grand__axe, demi_petite_axe):
    """
    create de la grille de coordonnées pour le modèle globale de la terre
    
    """
    #ellipsoide: a*x**2 + b*y**2 + c*z**2 = 1 
    #ellipsoide de révolution: a=b
    a, b, c = demi_grand__axe, demi_grand__axe, demi_petite_axe

    # Set of all spherical angles:
    lambda_deg = np.linspace(-179, 179, 30) # lon
    phi_deg = np.linspace(-89, 89, 30)   # lat
    

    lon, lat = np.meshgrid(lambda_deg, phi_deg, copy = False)

    rad = np.pi/180
    x = a * np.cos(lon * rad )*np.cos(lat * rad)
    y = b * np.sin(lon * rad)*np.cos(lat * rad)
    z = c * np.sin(lat * rad)

   

    return (x, y, z, lon, lat)
