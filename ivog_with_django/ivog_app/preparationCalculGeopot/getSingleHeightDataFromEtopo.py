import numpy as np

from ivog_app.calcul.champEtEllipsoid.getTopoCoordFromGeoportail import get_elevation_from_geoportail
from ivog_app.calcul.champEtEllipsoid.fileCreatorForGeopot import createInputFileFromCoordinateForGeopot
from ivog_app.calcul.champEtEllipsoid.autoGeopot import autoComputeChampsPotentiel

from ivog_app.calcul.champEtEllipsoid.getTopoCoordinateFromEtopo import get_elevation
# from .createSaveGMTView import observable_index_champs

def calculSurUnPoint(lon, lat, resolution, modele):
    """
    Calcul les observable en un point
    lon et lat en degré
    resolution:resolution de calcul pour geopot
    """

    h = get_elevation_from_geoportail(lon, lat)
    if (h == -99999):
        h = get_elevation(lon, lat)

    lon_geopot = np.array([lon]).reshape((-1, 1))
    lat_geopot = np.array([lat]).reshape((-1, 1))
    h = np.array([h]).reshape((-1, 1))

    createInputFileFromCoordinateForGeopot(lon_geopot, lat_geopot, h, filenamme= "singlePointObs")
    autoComputeChampsPotentiel("singlePointObs", resolution, modele)

def calculSurUnPointForAlti(lon, lat):

    h = get_elevation_from_geoportail(lon, lat)
    if (h == -99999):
        h = get_elevation(lon, lat)

    lon_geopot = np.array([lon]).reshape((-1, 1))
    lat_geopot = np.array([lat]).reshape((-1, 1))
    h = np.array([h]).reshape((-1, 1))
    return(h, lon_geopot, lat_geopot)


