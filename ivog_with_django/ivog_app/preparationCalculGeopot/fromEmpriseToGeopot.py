import os
from ivog_app.calcul.champEtEllipsoid.autoGeopot import autoComputeChampsPotentiel
from ivog_app.calcul.champEtEllipsoid.getEmpriseTopoFromEtopo import getEmpriseDataTopo
from ivog_app.calcul.champEtEllipsoid.fileCreatorForGeopot import createInputFileFromCoordinateForGeopot


def EmpriseToGeopotoutput(lng1 = 4,lng2 = 7,lat1 = 46, lat2 = 48,  resolution = "360", modele = "eigen6c4"):
    """
    """

    cwd = os.getcwd() #où est initialiser le chemin du terminal 
    pwd = os.path.dirname(__file__) #où ce trouve ce script
    # Change the current working directory wherer this script is 
    os.chdir(pwd)
    try:
        matrixLat, matrixLng, matrixHauteur, shapeGrille = getEmpriseDataTopo(lng1,lng2,lat1, lat2)
        # print("""LALALALALALALAL""", matrixLat)
        createInputFileFromCoordinateForGeopot(matrixLng, matrixLat, matrixHauteur, filenamme= "emprisePointObs")
        autoComputeChampsPotentiel("emprisePointObs", resolution, modele)
        os.chdir(cwd)
        return (shapeGrille)
    except:
        #si echec, on prendra les données d'initialisation de l'interface
        print("si echec, on prend les données d'initialisation de l'interface")
        os.chdir(cwd)
        return (0, 0)
    
# listPointToGeopotoutput()

def EmpriseToGeopotoutputForAlti(lng1 = 4,lng2 = 7,lat1 = 46, lat2 = 48):
    """
    """
    cwd = os.getcwd() #où est initialiser le chemin du terminal 
    pwd = os.path.dirname(__file__) #où ce trouve ce script
    # Change the current working directory wherer this script is 
    os.chdir(pwd)
    try:
        matrixLat, matrixLng, matrixHauteur, shapeGrille = getEmpriseDataTopo(lng1,lng2,lat1, lat2)
        os.chdir(cwd)
        return (matrixLng, matrixLat, matrixHauteur, shapeGrille)
    except:
        #si echec, on prendra les données d'initialisation de l'interface
        print("si echec, on prend les données d'initialisation de l'interface")
        os.chdir(cwd)
        return(0, 0, 0, 0)

