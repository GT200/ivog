import os
import numpy as np
from io import StringIO

from ivog_app.calcul.champEtEllipsoid.autoGeopot import autoComputeChampsPotentiel
from ivog_app.calcul.champEtEllipsoid.getTopoCoordinateFromEtopo import get_elevation
from ivog_app.calcul.champEtEllipsoid.getTopoCoordFromGeoportail import get_elevation_from_geoportail
from ivog_app.calcul.champEtEllipsoid.fileCreatorForGeopot import createInputFileFromCoordinateForGeopot

# cwd = os.getcwd() #to run sh files
# pwd = os.path.dirname(__file__) #replace present Wording directory
# path_parent = os.path.dirname(pwd)
# os.chdir(path_parent + "/ivog_app.calcul.champEtEllipsoid")
# print(path_parent)

def listPointToGeopotoutput(stringLngLat = "4.5352  50.6022\n1.5908  49.3321", resolution = "360", modele = "eigen6c4"):
    """
    recuperateur de données sur les api en font de long et lat
    """
    cwd = os.getcwd() #où est initialiser le chemin du terminal 
    pwd = os.path.dirname(__file__) #où ce trouve ce script
    # Change the current working directory wherer this script is 
    os.chdir(pwd)

    try:
        data = StringIO(stringLngLat)
        lnglat = np.genfromtxt(data, dtype=float)
        # print(lnglat.shape)
        matrixLng = lnglat[:, 0:1]
        matrixLat = lnglat[:, 1:2]
        matrixHauteur= np.zeros(matrixLat.shape)
        for i in range(matrixLng.shape[0]):
            lng = float(matrixLng[i, 0])
            lat = float(matrixLat[i, 0])
            h = get_elevation_from_geoportail(lng, lat)
            if (h == -99999): 
                h = get_elevation(lng, lat)
            matrixHauteur[i, 0] = h
    except:
        #si echec, on prend les données d'initialisation de l'interface
        lnglath = np.genfromtxt("../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/donneesAuDemarrage/geopot_output_eigen_multiPointObs_FR",  usecols= (0, 1, 2))
        matrixLat= lnglath[:, 0:1]
        matrixLng = lnglath[:, 1:2]
        matrixHauteur= lnglath[:, 2:3]

    # print(matrixLng, matrixLat, matrixHauteur, sep= "\n")

    createInputFileFromCoordinateForGeopot(matrixLng, matrixLat, matrixHauteur, filenamme= "multiPointObs")
    autoComputeChampsPotentiel("multiPointObs", resolution, modele)
    os.chdir(cwd)

# listPointToGeopotoutput()


def listPointToGeopotoutputForAlti(stringLngLat = "4.5352  50.6022\n1.5908  49.3321"):
    """
    """
    cwd = os.getcwd() #où est initialiser le chemin du terminal 
    pwd = os.path.dirname(__file__) #où ce trouve ce script
    # Change the current working directory wherer this script is 
    os.chdir(pwd)
    try:
        data = StringIO(stringLngLat)
        lnglat = np.genfromtxt(data, dtype=float)
        # print(lnglat.shape)
        matrixLng = lnglat[:, 0:1]
        matrixLat = lnglat[:, 1:2]
        matrixHauteur= np.zeros(matrixLat.shape)
        for i in range(matrixLng.shape[0]):
            lng = float(matrixLng[i, 0])
            lat = float(matrixLat[i, 0])
            h = get_elevation_from_geoportail(lng, lat)
            if (h == -99999): 
                h = get_elevation(lng, lat)
            matrixHauteur[i, 0] = h
    except:
        #si echec, on prend les données d'initialisation de l'interface
        print("si echec, on prend les données d'initialisation de l'interface")
        lnglath = np.genfromtxt("../calcul/alti/altiData/auDemarrage/altiListePoint_FR", usecols= (0, 1, 2))
        matrixLat= lnglath[:, 0:1]
        matrixLng = lnglath[:, 1:2]
        matrixHauteur= lnglath[:, 2:3]
    os.chdir(cwd)
    return(matrixLng, matrixLat, matrixHauteur)

