import numpy as np
import os 

from ivog_app.managerDonneeAffichage.donneeDictionnaire import observable_index_champ

observable_index= observable_index_champ

def getSinglePointData(premiereConnection):
    """"
    recupère les données du dernier calcul fait sur ivog et prépare le fichier de téléchargement

    premiereConnection: bouléen. C'est la première ouverture de ivog => points centrer en france

    """

    global observable_index

    cwd = os.getcwd() #où est initialiser le chemin du terminal 
    pwd = os.path.dirname(__file__) #où ce trouve ce script

    # Change the current working directory wherer this script is 
    os.chdir(pwd)

    try:
        geopotLastPointData = np.genfromtxt("../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/geopot_output_eigen_singlePointObs")
        filenames =["../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/header_champ.txt", "../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/geopot_output_eigen_singlePointObs"]
    except:
        #si echec, on prend les données d'initialisation de l'interface
        print("si echec, on prend les données d'initialisation de l'interface")
        geopotLastPointData = np.genfromtxt("../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/donneesAuDemarrage/geopot_output_eigen_singlePointObs_FR")
        filenames =["../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/header_champ.txt", "../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/donneesAuDemarrage/geopot_output_eigen_singlePointObs_FR"]
    

    if(premiereConnection):
        # print("PREMIER CONENECTIONNNNNNN")
        geopotLastPointData = np.genfromtxt("../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/donneesAuDemarrage/geopot_output_eigen_singlePointObs_FR")
        filenames =["../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/header_champ.txt", "../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/donneesAuDemarrage/geopot_output_eigen_singlePointObs_FR"]
        
    
    with open('../telechargerDonnees/DonneesIVOGEnUnPoint.txt', 'w') as outfile:
        for x in filenames:
            with open(x) as infile:
                for line in infile:
                    outfile.write(line)
                outfile.write("\n")

    # print(geopotLastPointData)

    for numObs in range(1, 27):
        if (numObs not in [7, 8, 10, 11,18, 21, 22, 23,  24, 25, 26]):
            observable = float(geopotLastPointData[numObs - 1])
            observable_index[numObs]["value"] = observable
    os.chdir(cwd)
    return(observable_index)

