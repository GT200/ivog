import numpy as np
import os 

def getListPointData(premiereConnection):

    observables_Liste_Point = {}

    cwd = os.getcwd() #où est initialiser le chemin du terminal 
    pwd = os.path.dirname(__file__) #où ce trouve ce script

    # Change the current working directory wherer this script is 
    os.chdir(pwd)
    try:
        geopotLastPointData = np.genfromtxt("../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/geopot_output_eigen_multiPointObs")
        filenames =["../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/header_champ.txt", "../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/geopot_output_eigen_multiPointObs"]
    except:
        #si echec, on prend les données d'initialisation de l'interface
        print("si echec, on prend les données d'initialisation de l'interface")
        geopotLastPointData = np.genfromtxt("../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/donneesAuDemarrage/geopot_output_eigen_multiPointObs_FR")
        filenames =["../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/header_champ.txt", "../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/donneesAuDemarrage/geopot_output_eigen_multiPointObs_FR"]

    if(premiereConnection):
        # print("PREMIER CONENECTIONNNNNNN LISTE")
        geopotLastPointData = np.genfromtxt("../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/donneesAuDemarrage/geopot_output_eigen_multiPointObs_FR")
        filenames =["../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/header_champ.txt", "../calcul/champEtEllipsoid/iVOG_Geopot_Glion_v1_3_1/Workspace_Geopot/donneesAuDemarrage/geopot_output_eigen_multiPointObs_FR"]
        
    with open('../telechargerDonnees/DonneesIVOGEnUneLIsteDePoint.txt', 'w') as outfile:
        for x in filenames:
            with open(x) as infile:
                for line in infile:
                    outfile.write(line)
                outfile.write("\n")

    # print(geopotLastPointData)
    for nb_pt in range( geopotLastPointData.shape[0]):
        observable_index = {1:{"unit": "°", "title": "latitude géo", "symbole": "φ", "bg":"#8b4fc35e"}, 2:{"unit": "°", "title": "longitude géo", "symbole": "λ", "bg":"#8b4fc35e"}, 3:{"unit": "m", "title": "Hauteur ellipsoïdale Geoportail/Etopo1", "symbole": "h", "bg":"#8b4fc35e"}, 4:{"unit": "m²/s²", "title": "Potentiel gravitationnel total", "symbole": "V"}, 5:{"unit": "m²/s²", "title": "Potentiel axifuge total", "symbole": "ϕ"}, 6:{"unit": "m²/s²", "title": "Potentiel de pesanteur total", "symbole": "W"}, 9:{"unit": "m²/s²", "title": "Potentiel de pesanteur normal", "symbole": "U"}, 12:{"unit":"m²/s²", "title": "Potentiel de pesanteur perturbateur", "symbole": "T"}, 13:{"unit": "mGals", "title": "Perturbation de pesanteur (abs(g)-abs(gamma))", "symbole": "δ"}, 14:{"unit": "mGals", "title": " Anomalie de pesanteur", "symbole":"Δ"}, 15:{"unit": "arcsec", "title": "Déflection de la verticale direction N/S", "symbole":"ξ"}, 16:{"unit": "arcsec", "title": "Déflection de la verticale direction E/W", "symbole":"η"}, 17:{"unit": "m/sec²", "title": "Pesanteur totale à la hauteur h",  "symbole":"g_h"}, 19:{"unit": "m/s²", "title": "Pesanteur normale à la hauteur h (formule)", "symbole":"ɣ_h"}, 20:{"unit": "m", "title": "Anomalie d'altitude", "symbole":"ζ"}}
        for numObs in range(1, 27):
            if (numObs not in [7, 8, 10, 11,18, 21, 22, 23,  24, 25, 26]):
                observable = float(geopotLastPointData[nb_pt, numObs - 1])
                observable_index[numObs]["value"] = observable

        observables_Liste_Point["nb_pt" +str(nb_pt + 1)] = observable_index
    os.chdir(cwd)
    return(observables_Liste_Point)

# getListPointData()