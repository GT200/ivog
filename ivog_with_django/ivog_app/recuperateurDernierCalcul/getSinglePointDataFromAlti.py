import numpy as np
import os 

from ivog_app.managerDonneeAffichage.donneeDictionnaire import observable_index_alti

def getSinglePointAltiData(premiereConnection):
    global observable_index_alti
    cwd = os.getcwd() #où est initialiser le chemin du terminal 
    pwd = os.path.dirname(__file__) #où ce trouve ce script

    # Change the current working directory wherer this script is 
    os.chdir(pwd)
    try:
        altiLastPointData = np.genfromtxt("../calcul/alti/altiData/altiPoint")
        filenames =["../calcul/alti/altiData/header_alti.txt", "../calcul/alti/altiData/altiPoint"]
    except:
        #si echec, on prend les données d'initialisation de l'interface
        print("si echec, on prend les données d'initialisation de l'interface")
        altiLastPointData = np.genfromtxt("../calcul/alti/altiData/auDemarrage/altiPoint_FR")
        filenames =["../calcul/alti/altiData/header_alti.txt", "../calcul/alti/altiData/auDemarrage/altiPoint_FR"]

    if(premiereConnection):
        # print("PREMIER CONENECTIONNNNNNN")
        altiLastPointData = np.genfromtxt("../calcul/alti/altiData/auDemarrage/altiPoint_FR")
        filenames =["../calcul/alti/altiData/header_alti.txt", "../calcul/alti/altiData/auDemarrage/altiPoint_FR"]
    
    with open('../telechargerDonnees/DonneesIVOGaltiEnUnPoint.txt', 'w') as outfile:
        for x in filenames:
            with open(x) as infile:
                for line in infile:
                    outfile.write(line)
                outfile.write("\n")

    for numObs in range(1, 11):
        observable = float(altiLastPointData[numObs - 1])
        observable_index_alti[numObs]["value"] = observable
    os.chdir(cwd)
    return(observable_index_alti)

