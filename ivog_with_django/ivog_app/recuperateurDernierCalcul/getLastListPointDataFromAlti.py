import numpy as np
import os 


observable_index_alti = {1:{"unit": "°", "title": "latitude", "symbole": "φ", "bg":"#8b4fc35e"}, 2:{"unit": "°", "title": "longitude", "symbole": "λ", "bg":"#8b4fc35e"}, 3:{"unit": "m", "title": "Hauteur DEM Geoportail/Etopo1", "symbole": "h", "bg":"#8b4fc35e"},  4:{"unit": "m²/s²", "title": "Potentiel de pesanteur total", "symbole": "w", "bg":"#2d30c05e"}, 5:{"unit": "m", "title": "altitude dynamique", "symbole": "Hd", "bg":"#5b4fc35e"},  6:{"unit": "m", "title": "altitude normale", "symbole": "Hn", "bg":"#5b4fc35e"}, 7:{"unit": "m", "title": "altitude orthométrique", "symbole": "Ho", "bg":"#5b4fc35e"}, 8:{"unit": "m", "title": "diff dynamique-normale", "bg":"#4facc35e", "symbole": "Hd-Hn",}, 9:{"unit": "m", "title": "diff ortho-normale", "symbole": "Ho-Hn", "bg":"#4facc35e"}, 10:{"unit": "m", "title": "diff dynamique-ortho", "symbole": "Hd-Ho", "bg":"#4facc35e"}}
def getListPointAltiData(premiereConnection):
    global observable_index
    cwd = os.getcwd() #où est initialiser le chemin du terminal 
    pwd = os.path.dirname(__file__) #où ce trouve ce script

    # Change the current working directory wherer this script is 
    os.chdir(pwd)
    try:
        altiLastData = np.genfromtxt("../calcul/alti/altiData/altiListePoint")
        filenames =["../calcul/alti/altiData/header_alti.txt", "../calcul/alti/altiData/altiListePoint"]
    except:
        #si echec, on prend les données d'initialisation de l'interface
        print("si echec, on prend les données d'initialisation de l'interface")
        altiLastData = np.genfromtxt("../calcul/alti/altiData/auDemarrage/altiListePoint_FR")
        filenames =["../calcul/alti/altiData/header_alti.txt", "../calcul/alti/altiData/auDemarrage/altiListePoint_FR"]

    if(premiereConnection):
        # print("PREMIER CONENECTIONNNNNNN")
        altiLastData = np.genfromtxt("../calcul/alti/altiData/auDemarrage/altiListePoint_FR")
        filenames =["../calcul/alti/altiData/header_alti.txt", "../calcul/alti/altiData/auDemarrage/altiListePoint_FR"]
    
    with open('../telechargerDonnees/DonneesIVOGaltiEnUneListeDePoint.txt', 'w') as outfile:
        for x in filenames:
            with open(x) as infile:
                for line in infile:
                    outfile.write(line)
                outfile.write("\n")

    observables_Liste_Point = {}

    for nb_pt in range( altiLastData.shape[0]):
        observable_index_alti = {1:{"unit": "°", "title": "latitude", "symbole": "φ", "bg":"#8b4fc35e"}, 2:{"unit": "°", "title": "longitude", "symbole": "λ", "bg":"#8b4fc35e"}, 3:{"unit": "m", "title": "Hauteur DEM Geoportail/Etopo1", "symbole": "h", "bg":"#8b4fc35e"},  4:{"unit": "m²/s²", "title": "Potentiel de pesanteur total", "symbole": "w", "bg":"#2d30c05e"}, 5:{"unit": "m", "title": "altitude dynamique", "symbole": "Hd", "bg":"#5b4fc35e"},  6:{"unit": "m", "title": "altitude normale", "symbole": "Hn", "bg":"#5b4fc35e"}, 7:{"unit": "m", "title": "altitude orthométrique", "symbole": "Ho", "bg":"#5b4fc35e"}, 8:{"unit": "m", "title": "diff dynamique-normale", "bg":"#4facc35e", "symbole": "Hd-Hn",}, 9:{"unit": "m", "title": "diff ortho-normale", "symbole": "Ho-Hn", "bg":"#4facc35e"}, 10:{"unit": "m", "title": "diff dynamique-ortho", "symbole": "Hd-Ho", "bg":"#4facc35e"}}
        for numObs in range(1, 11):
            observable = float(altiLastData[nb_pt, numObs - 1])
            observable_index_alti[numObs]["value"] = observable
        observables_Liste_Point["nb_pt" +str(nb_pt + 1)] = observable_index_alti
    os.chdir(cwd)
    return(observables_Liste_Point)
