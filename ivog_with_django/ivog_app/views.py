from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect

from ivog_with_django import settings

from .recuperateurDernierCalcul.getLastEmpriseDataFromAlti import getEmpriseAltiData

from .recuperateurDernierCalcul.getLastListPointDataFromAlti import getListPointAltiData

from .calcul.alti.altitude import alti

from .calcul.champEtEllipsoid.CompareEllipsoid import calcul_diff_ellips, compare2eps, getElipsoidNames

from .recuperateurDernierCalcul.getSinglePointDataFromAlti import getSinglePointAltiData

from .createSaveGMTView import createAllObsImgEmrpise, createAllObsImgEmrpiseAlti

from .managerDonneeAffichage.donneeDictionnaire import observable_index_champ, observable_index_alti

from .preparationCalculGeopot.fromEmpriseToGeopot import EmpriseToGeopotoutput, EmpriseToGeopotoutputForAlti

from .recuperateurDernierCalcul.getLastEmpriseData import getEmpriseData

from .preparationCalculGeopot.fromListPointivogToGeopetoutput import listPointToGeopotoutput, listPointToGeopotoutputForAlti

from .recuperateurDernierCalcul.getLastListPointDataFromGeopot import getListPointData

from .recuperateurDernierCalcul.getLastPointDataFromGeopot import getSinglePointData

# from django.core.mail import send_mail

from .preparationCalculGeopot.getSingleHeightDataFromEtopo import calculSurUnPoint, calculSurUnPointForAlti
import pyximport
pyximport.install()

# Import mimetypes module
import mimetypes
# import os module
import os
# Import HttpResponse module
from django.http.response import HttpResponse
# Import render module
from django.shortcuts import render

def index(request):
    """
    Fonction de gestion d'affichage de la page d'accueil avec la spécificité du language django

    return: appel à la page d'accueil HTML et envoie des données dans cette page
    
    """
    #importation des données pour l'accueil
    from .managerDonneeAffichage.donneeDictionnaire import topNavBarContent, topNavBarUrl, obs_accueil
    class_page = ["header", "header","header", "header", "header", "header"]
    class_page[0] += " active" #header onglet accueil active 
    #préparation de l'envoie des données dans la page d'accueil avec le dictionnaire context
    context = {'currenturl': 'index', "topNavData":[{"name":topNavBarContent[0] , "class": class_page[0], "url" : topNavBarUrl[0]}, {"name":topNavBarContent[1] , "class": class_page[1], "url" : topNavBarUrl[1]}, {"name":topNavBarContent[2] , "class": class_page[2] ,"url" : topNavBarUrl[2]}, {"name":topNavBarContent[3] , "class": class_page[3], "url" : topNavBarUrl[3]}, {"name":topNavBarContent[4] , "class": class_page[4], "url" : topNavBarUrl[4]}, {"name":topNavBarContent[5] , "class": class_page[5], "url" : topNavBarUrl[5]}]}
    context['obs'] = obs_accueil
    #envoie des données et apper à la page d'accueil
    return render(request, 'ivog_app/index.html', context)

@csrf_protect 
def surfaceDeReference(request):
    """
    Fonction de gestion d'affichage de la page d'surface de référance avec la spécificité du language django

    return: appel à la page d'accueil HTML et envoie des données dans cette page
    """
    from .calcul.champEtEllipsoid.ellipsoid import createEllipsoidGlobalGrid
    # demi_grand__axe, demi_petite_axe= 6378249.2, 6356515 #clarke ign a = 6378249.2m, b 6356515m
    # a, b, c = demi_grand__axe, demi_grand__axe, demi_petite_axe
    empsRefName = "WGS84" #ellipsoid de référence
    a, c = calcul_diff_ellips(empsRefName)
    b = a
    clickChoice= [1, 0, 0]

    from .managerDonneeAffichage.donneeDictionnaire import topNavBarContent, topNavBarUrl
    class_page = ["header", "header","header", "header", "header", "header"]
    class_page[1] += " active"

    #préparation des données deu header de référence dans la liste topNavData
    topNavData = [{"name":topNavBarContent[0] , "class": class_page[0], "url" : topNavBarUrl[0]}, {"name":topNavBarContent[1] , "class": class_page[1], "url" : topNavBarUrl[1]}, {"name":topNavBarContent[2] , "class": class_page[2] ,"url" : topNavBarUrl[2]}, {"name":topNavBarContent[3] , "class": class_page[3], "url" : topNavBarUrl[3]}, {"name":topNavBarContent[4] , "class": class_page[4], "url" : topNavBarUrl[4]}, {"name":topNavBarContent[5] , "class": class_page[5], "url" : topNavBarUrl[5]}]

    if request.method == "POST":
        data = request.POST
        empsRefName = data.get("ellipsoidRef")
        a, c = calcul_diff_ellips(empsRefName)
        b = a
        compare2eps(empsRefName)

    (x, y, z) = createEllipsoidGlobalGrid(a, c)[:3]

    surfcolor = (a*x**2 + b*y**2 + c*z**2)

    ellips_data = [x.tolist(), y.tolist(),z.tolist(),surfcolor.tolist()]

    context = {'currenturl': 'surfaceDeReference'}
    context["topNavData"] = topNavData 

    context["ellips_data"] = ellips_data
    context["modele"] = empsRefName
    context["clickChoice"] =  [1, 0, 0] #cliquer le pier bouton (eppsoid de référence au réfraichissement de la page)
    context["ellipsoidNames"] = getElipsoidNames()

    try:
        invf = a/(a-c)
    except:
        invf = "infini pour une sphère"

    context["paramEpsRef"] = {"name": empsRefName, "demi_grand__axe":a, "demi_petite_axe": b, "applatissementInv": invf}

    return render(request, 'ivog_app/surfRef.html', context)

def champs(request):
    """
    Fonction de gestion d'affichage de la page de champ de pesenateur avec la spécificité du language django

    return: appel à la page d'accueil HTML et envoie des données dans cette page
    """

    from .managerDonneeAffichage.donneeDictionnaire import topNavBarContent, topNavBarUrl, obsGMTChampsPesanteur, num_observable_name_champs, obsGMTChampsPesanteurPremierDemarrage
    class_page = ["header", "header","header", "header", "header", "header"]
    obsGMT = obsGMTChampsPesanteurPremierDemarrage
    imgGMTDemanrage = 'ivog_app/img/obs_champs_potentiel/auDemarrage/obs_champs_potentiel/obsEmrprise3.png'

    class_page[2] += " active"

    topNavData = [{"name":topNavBarContent[0] , "class": class_page[0], "url" : topNavBarUrl[0]}, {"name":topNavBarContent[1] , "class": class_page[1], "url" : topNavBarUrl[1]}, {"name":topNavBarContent[2] , "class": class_page[2] ,"url" : topNavBarUrl[2]}, {"name":topNavBarContent[3] , "class": class_page[3], "url" : topNavBarUrl[3]}, {"name":topNavBarContent[4] , "class": class_page[4], "url" : topNavBarUrl[4]}, {"name":topNavBarContent[5] , "class": class_page[5], "url" : topNavBarUrl[5]}]

    #varibale pour savoir si on cacul ou pas les observable
    compute_point = False 
    compute_list_point = False
    compute_emprise= False

    clickChoice= [1, 0, 0] #cliquer un bouton (point, liste de point, emprise ) à l'ouverture

    if request.method == "POST":
        data = request.POST
        if (data.get("mutliPointInput") is None and data.get("lng1") is None):
            try:
                lon = float(data.get("Longitude"))
                lat = float(data.get("Latitude"))
            except:
                lon = data.get("Longitude")
                lat = data.get("Latitude")

            resolution = data.get("résolution")
            modele = data.get("modeles")
            compute_point = True
        elif (data.get("Longitude") is None and data.get("lng1") is None):
            clickChoice= [0, 1, 0]
            charlnglat =data.get("mutliPointInput")
            resolution = data.get("résolution")
            modele = data.get("modeles")
            compute_list_point = True
        else: 
            clickChoice= [0, 0, 1]
            try:
                lng1 = float(data.get("lng1"))
                lng2 = float(data.get("lng2"))
                lat1 = float(data.get("lat1"))
                lat2 = float(data.get("lat2"))
            except:
                lng1 = data.get("lng1")
                lng2 = data.get("lng2")
                lat1 = data.get("lat1")
                lat2 = data.get("lat2")
            resolution = data.get("résolution")
            modele = data.get("modeles")
            compute_emprise = True
            obsGMT = obsGMTChampsPesanteur
            imgGMTDemanrage = 'ivog_app/img/obs_champs_potentiel/obsEmrprise3.png'

    #calcul single point
    context = {'currenturl': 'champs'}

    if (not compute_point):
        context["DataFromPoint"] = getSinglePointData(True)
        
    else:
        try:
            calculSurUnPoint(lon, lat, resolution, modele)
            context["DataFromPoint"] = getSinglePointData(False)
        except:
            context["DataFromPoint"] = getSinglePointData(True)
    if (not compute_list_point):
        context["DataFromList"] = getListPointData(True)
    else:
        try:
            listPointToGeopotoutput(charlnglat, resolution, modele)
            context["DataFromList"] = getListPointData(False)
        except:
            context["DataFromList"] = getListPointData(True)
    if (not compute_emprise):
        context["DataFromEmprise"] = getEmpriseData(True)
    else:
        try:
            shapeGrille = EmpriseToGeopotoutput(lng1,lng2 ,lat1 , lat2, resolution, modele)
            createAllObsImgEmrpise(shapeGrille, observable_index_champ)
            context["DataFromEmprise"] = getEmpriseData(False)
        except:
            context["DataFromEmprise"] = getEmpriseData(True)


    context["obsNameFromPointData"] =observable_index_champ.values() #récupération des non des obs
    context["topNavData"] = topNavData
    context["obsGMT"] = obsGMT
    context["imgGMTDemanrage"] = imgGMTDemanrage
    context["clickChoice"] = clickChoice

    context["num_observable_name_champs"] = num_observable_name_champs
    context["telechargerObsEmrpise"] = 'DonneesIVOGSurUneEmprise.txt'
    context["telechargerObsLP"] = 'DonneesIVOGEnUneLIsteDePoint.txt'
    context["telechargerObsPoint"] = 'DonneesIVOGEnUnPoint.txt'


    return render(request, 'ivog_app/champs.html', context)

def altitudes(request):
    """
    Fonction de gestion d'affichage de la page de'alti avec la spécificité du language django

    return: appel à la page d'accueil HTML et envoie des données dans cette page
    """

    from .managerDonneeAffichage.donneeDictionnaire import topNavBarContent, topNavBarUrl, num_observable_name_alti, obsGMTalti, observable_index_alti, obsGMTaltiDemarrage

    class_page = ["link active", "link","link", "link", "link", "link"]

    class_page[3] += " active"
    topNavData = [{"name":topNavBarContent[0] , "class": class_page[0], "url" : topNavBarUrl[0]}, {"name":topNavBarContent[1] , "class": class_page[1], "url" : topNavBarUrl[1]}, {"name":topNavBarContent[2] , "class": class_page[2] ,"url" : topNavBarUrl[2]}, {"name":topNavBarContent[3] , "class": class_page[3], "url" : topNavBarUrl[3]}, {"name":topNavBarContent[4] , "class": class_page[4], "url" : topNavBarUrl[4]}, {"name":topNavBarContent[5] , "class": class_page[5], "url" : topNavBarUrl[5]}]

    obsGMT = obsGMTaltiDemarrage

    imgGMTDemanrage = 'ivog_app/img/obsAlti/auDemarrage/obsAlti3.png'


    compute_point = False
    compute_list_point = False
    compute_emprise= False

    clickChoice= [1, 0, 0]

    if request.method == "POST":
        data = request.POST
        if (data.get("mutliPointInput") is None and data.get("lng1") is None):
            lon = float(data.get("Longitude"))
            lat = float(data.get("Latitude"))
            resolution = data.get("résolution")
            modele = data.get("modeles")
            compute_point = True
            # print(lon)
        elif (data.get("Longitude") is None and data.get("lng1") is None):
            clickChoice= [0, 1, 0]
            charlnglat =data.get("mutliPointInput")
            resolution = data.get("résolution")
            modele = data.get("modeles")
            compute_list_point = True
        else: 
            clickChoice= [0, 0, 1]
            lng1 = float(data.get("lng1"))
            lng2 = float(data.get("lng2"))
            lat1 = float(data.get("lat1"))
            lat2 = float(data.get("lat2"))
            resolution = data.get("résolution")
            modele = data.get("modeles")
            imgGMTDemanrage = 'ivog_app/img/obsAlti/obsAlti3.png'
            obsGMT = obsGMTalti

            compute_emprise = True

    #calcul single point
    context = {'currenturl': 'alti'}

    context["obsNameFromPointData"] = observable_index_alti.values()
    if (not compute_point):
        context["DataFromPoint"] = getSinglePointAltiData(True)
    else:
        (h, lon, lat) = calculSurUnPointForAlti(lon, lat)
        alti(lon, lat, h, "Point", resolution, modele)
        context["DataFromPoint"] = getSinglePointAltiData(False)
        
    if (not compute_list_point):
        context["DataFromList"] = getListPointAltiData(True)
    else:
        (lon, lat, h) = listPointToGeopotoutputForAlti(charlnglat)
        alti(lon, lat, h, "ListePoint", resolution, modele)
        context["DataFromList"] = getListPointAltiData(False)
    if (not compute_emprise):
        context["DataFromEmprise"] = getEmpriseAltiData(True)
    else:
        # print(lng1,lng2 ,lat1 , lat2)
        try:
            lon, lat, h, shapeGrille = EmpriseToGeopotoutputForAlti(lng1,lng2 ,lat1 , lat2)
            alti(lon, lat, h, "Emprise", resolution, modele)
            createAllObsImgEmrpiseAlti(shapeGrille, observable_index_alti)
            context["DataFromEmprise"] = getEmpriseAltiData(False)
        except:
            context["DataFromEmprise"] = getEmpriseAltiData(True)
   
    context["topNavData"] = topNavData
    context["obsGMT"] = obsGMT
    context["clickChoice"] = clickChoice
    context["num_observable_name_champs"] = num_observable_name_alti
    context["imgGMTDemanrage"] = imgGMTDemanrage

    context["telechargerObsEmrpise"] = 'DonneesIVOGaltiSurUneEmprise.txt'
    context["telechargerObsLP"] = 'DonneesIVOGaltiEnUneListeDePoint.txt'
    context["telechargerObsPoint"] = 'DonneesIVOGaltiEnUnPoint.txt'


    return render(request, 'ivog_app/altitudes.html', context)


def equipe(request):

    """
    Fonction de gestion d'affichage de la page de'équipe avec la spécificité du language django

    return: appel à la page d'accueil HTML et envoie des données dans cette page
    """

    from .managerDonneeAffichage.donneeDictionnaire import topNavBarContent, topNavBarUrl
    class_page = ["link active", "link","link", "link", "link", "link"]

    class_page[4] += " active"
    topNavData = [{"name":topNavBarContent[0] , "class": class_page[0], "url" : topNavBarUrl[0]}, {"name":topNavBarContent[1] , "class": class_page[1], "url" : topNavBarUrl[1]}, {"name":topNavBarContent[2] , "class": class_page[2] ,"url" : topNavBarUrl[2]}, {"name":topNavBarContent[3] , "class": class_page[3], "url" : topNavBarUrl[3]}, {"name":topNavBarContent[4] , "class": class_page[4], "url" : topNavBarUrl[4]}, {"name":topNavBarContent[5] , "class": class_page[5], "url" : topNavBarUrl[5]}]



    context = {'currenturl': 'equipe'}
    context["topNavData"] = topNavData

    return render(request, 'ivog_app/equipe.html', context)


def contact(request):
    """
    Fonction de gestion d'affichage de la page de avec la spécificité du language django

    return: appel à la page d'accueil HTML et envoie des données dans cette page
    """
    from .managerDonneeAffichage.donneeDictionnaire import topNavBarContent, topNavBarUrl
    class_page = ["link active", "link","link", "link", "link", "link"]

    class_page[5] += " active "
    topNavData = [{"name":topNavBarContent[0] , "class": class_page[0], "url" : topNavBarUrl[0]}, {"name":topNavBarContent[1] , "class": class_page[1], "url" : topNavBarUrl[1]}, {"name":topNavBarContent[2] , "class": class_page[2] ,"url" : topNavBarUrl[2]}, {"name":topNavBarContent[3] , "class": class_page[3], "url" : topNavBarUrl[3]}, {"name":topNavBarContent[4] , "class": class_page[4], "url" : topNavBarUrl[4]}, {"name":topNavBarContent[5] , "class": class_page[5], "url" : topNavBarUrl[5]}]

    context = {'currenturl': 'contact'}
    context["topNavData"] = topNavData

    return render(request, 'ivog_app/contact.html', context)




# Define function to download pdf file using template
def download(request, filename=''):
    if filename != '':
        # Define Django project base directory
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        # Define the full file path
        filepath = BASE_DIR + '/ivog_app/telechargerDonnees/' + filename
        # Open the file for reading content
        path = open(filepath, 'rb')
        # Set the mime type

        mime_type, _ = mimetypes.guess_type(filepath)
        # Set the return value of the HttpResponse
        response = HttpResponse(path, content_type=mime_type)
        # Set the HTTP header for sending to browser
        response['Content-Disposition'] = "attachment; filename=%s" % filename
        # Return the response value
        return response
    else:
        return render(request, 'ivog_app/contact.html')
        