# iVOG

## Name (web app)
iVOG
(https://gt200.pythonanywhere.com/)

## Description
-----------------------README-----------------------
iVoG est une interface web sur la géodésie
créée dans le cadre du projet développement
informatique de l'ENSG pour la journée
Geodev2.
Il permet de faire différents calculs
d'observables géodésiques (altitudes, longitude,
latitude,...) et visualiser les ellipsoïdes et géoïdes
tout en apportant un point de vue pédagogique à la
géodésie.
Le site est fait de manière à ce quelqu'un de novice
dans le domaine puisse se renseigner et comprendre
la géodésie mais aussi quelqu'un d'expérimenté
dans le domaine puisse faire les calculs de façon
ergonomique.

Vous pourrez trouver le guide d'installation dans le
document install.txt et une vidéo tutoriel de comment
le site s'utilise.
## Visuals
-----------------------SITE WEB-----------------------
Le lien pour accéder à iVoG (lien modifiable):
https://gt200.pythonanywhere.com/

## Installation
---------------GUIDE D'INSTALLATION D'iVoG---------------

/////////////////Installation sous Linux/////////////////

Afin d'installer iVoG sur votre machine, il vous fera
nécessairement avoir :
- Conda (miniconda ou anaconda)
- Python
- Visual Studio Code (optionnel)

--> Aller sur : https://gitlab.com/GT200/ivog/
et faire un clone repository dans visual studio code :
(git clone) https://gitlab.com/GT200/ivog.git

Une fois que vous avez iVoG sur votre machine, pour le
faire fonctionner correctement, certaines installations
doivent se faire à l'aide d'un terminal :

- l'environnement pygmt :
conda create --name pygmt --channel conda-forge pygmt
conda activate pygmt
(plus de renseignements sur l'environnement pygmt sur
https://www.pygmt.org/latest/install.html)

Vérifiez que vous êtes bien placés dans le dossier ivog/ivog_with_django.

Après avoir activé pygmt, il est possible d'installer pyproj :
conda install pyproj

Puis d'autres dépendances et libraires vont éventuellement
être demandées, elles sont présentes dans le fichier
requirements.txt et lancez la ligne de commande suivante 
toujours au niveau du dossier ivog_with_django :

python3 install -r requirements.txt

Ainsi, vous pouvez lancer iVoG en local (toujours au niveau
du même dossier) :
python3 manage.py runserver

Vous pouvez cliquer/copier sur le lien qui vous permettra de
visualiser iVoG en local (localhost:8000) sur un navigateur web.

Bonne installation !


## Usage
Visuaisaation d'entité géodésique: 
    -surface de référece: éllipsoide, géoïde
    -champs de pesenateur
    -les altitudes 

## Support
-----------------------CONTACT-----------------------
Si vous avez des problèmes, des questions ou des
suggestions, n'hésitez pas à nous envoyer un message
(les mails sont dans les crédits).

-------------------------GIT--------------------------
Le lien pour accéder au GIT d'iVoG :
https://gitlab.com/GT200/ivog

-------------------------BUGS-------------------------

## Authors and acknowledgment
------------------------CREDITS------------------------
Commanditaire du projet : Guillaume Lion, glion@ipgp.fr

Développeurs :
Axel Losco, axel.losco@ensg.eu
Gaëtan Tiazara, gaetan.tiazara@ensg.eu
Jade Ternier, jade.ternier@ensg.eu
Tasnîme Louartani, tasnime.louartani@ensg.eu


## License
ENSG/IPGP/IGN

## Project status
Closed
